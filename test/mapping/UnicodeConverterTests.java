package mapping;

import static org.junit.Assert.*;
import org.junit.jupiter.api.Test;

class UnicodeConverterTests {

	@Test
	public void testConvertZero() {
		String hex = UnicodeConverter.convertIntToHex(0);
		assertEquals("Test converting 0 to hex", "\\u0000", hex);
	}
	
	@Test
	public void testConvertNegative() {
		String hex = UnicodeConverter.convertIntToHex(-2);
		//The unsigned integer value is the argument plus 2^32 if the argument is negative
		assertEquals("Test converting negative to hex", "\\uffffffe", hex);
	}
	
	@Test
	public void testCovertNumberLessThan16Bits() {
		String hex = UnicodeConverter.convertIntToHex(500);
		assertEquals("Test converting number less than 16 bits to hex", "\\u01f4", hex);
	}
	
	@Test
	public void testConvertNumberMoreThan16Bits() {
		String hex = UnicodeConverter.convertIntToHex(200000);
		assertEquals("Test converting number more than 16 bits to hex", "\\u30d40", hex);
	}
	
	@Test
	public void testConvertMaxInt() {
		String hex = UnicodeConverter.convertIntToHex(Integer.MAX_VALUE);
		assertEquals("Test converting the maximum integer to hex", "\\u7fffffff", hex);
	}
	
	@Test void testConvertMinInt() {
		String hex = UnicodeConverter.convertIntToHex(Integer.MIN_VALUE);
		assertEquals("Test converting the maximum integer to hex", "\\u0010000", hex);
	}
	
	@Test
	public void testConvertNumberExactly16Bits() {
		String hex = UnicodeConverter.convertIntToHex(65535);
		assertEquals("Test converting the maximum 16 bit number to hex", "\\uffff", hex);
	}
	
	@Test
	public void testConvertStringZero() {
		int character = UnicodeConverter.convertHexToInt("\\u0000");
		assertEquals("Test converting hex of 0 to int", 0, character);
	}
	
	@Test
	public void testConvertStringLessThan16Bits() {
		int character = UnicodeConverter.convertHexToInt("\\u006e");
		assertEquals("Test converting hex of less than 16 bit to int", 110, character);
	}
	
	@Test
	public void testConvertStringMoreThan16Bits() {
		int character = UnicodeConverter.convertHexToInt("\\u4f7e33");
		assertEquals("Test converting hex of more than 16 bit to int", 5209651, character);
	}
	
	@Test
	public void testConvertStringExactly16Bits() {
		int character = UnicodeConverter.convertHexToInt("\\uffff");
		assertEquals("Test converting the maximum 16 bit hex to int", 65535, character);
	}
	
	@Test
	public void testConvertStringMaxValue() {
		int character = UnicodeConverter.convertHexToInt("\\u7fffffff");
		assertEquals("Test converting the maximum value of hex to int", Integer.MAX_VALUE, character);
	}
	
	@Test
	public void testConvertStringOutOfRange() {
		int character = UnicodeConverter.convertHexToInt("\\u80000000");
		assertEquals("Test hex string larger than the maximum int returns 0", -1, character);
	}
	
	@Test
	public void testConvertInvalidString() {
		int character = UnicodeConverter.convertHexToInt("\\u023g");
		assertEquals("Test converting invalid hex to int, with numbers", -1, character);
		character = UnicodeConverter.convertHexToInt("\\gx");
		assertEquals("Test converting invalid hex to int, without numbers", -1, character);
	}
	
	@Test
	public void testConvertNullString() {
		int character = UnicodeConverter.convertHexToInt(null);
		assertEquals("Test converting null hex to int", -1, character);
	}
	
	@Test
	public void testConvertEmptyString() {
		int character = UnicodeConverter.convertHexToInt("");
		assertEquals("Test converting empty hex to int", -1, character);
	}
	
	@Test
	public void testConvertUnicodeIdentifier() {
		int character = UnicodeConverter.convertHexToInt("\\u");
		assertEquals("Test converting unicode identifier without hex", -1, character);
	}
	
	@Test
	public void testIsHexLessThan16Bits() {
		boolean isHex = UnicodeConverter.isUnicodeHexString("\\u006e");
		assertTrue("Test hex of less than 16 bit returns true", isHex);
	}
	
	@Test
	public void testIsHexMoreThan16Bits() {
		boolean isHex = UnicodeConverter.isUnicodeHexString("\\u1F600");
		assertTrue("Test hex of more than 16 bits returns true", isHex);
	}
	
	@Test
	public void testisHexExactly16Bits() {
		boolean isHex = UnicodeConverter.isUnicodeHexString("\\uffff");
		assertTrue("Test hex of the maximum 16 bit number returns true", isHex);
	}
	
	@Test
	public void testIsHexInvalidString() {
		boolean isHex = UnicodeConverter.isUnicodeHexString("\\u023g");
		assertFalse("Test invalid hex returns false", isHex);
	}
	
	@Test
	public void testIsHexNullString() {
		boolean isHex = UnicodeConverter.isUnicodeHexString(null);
		assertFalse("Test null hex string returns false", isHex);
	}
	
	@Test
	public void testIsHexEmptyString() {
		boolean isHex = UnicodeConverter.isUnicodeHexString("");
		assertFalse("Test empty hex to returns false", isHex);
	}
	
	@Test
	public void testIsHexUnicodeIdentifier() {
		boolean isHex = UnicodeConverter.isUnicodeHexString("\\u");
		assertFalse("Test unicode identifier without hex returns false", isHex);
	}
	
	@Test
	public void testIsHexNumberLargerThanUnicodeMax() {
		boolean isHex = UnicodeConverter.isUnicodeHexString("\\uffffff");
		assertFalse("Test unicode larger than the max unicode character returns false", isHex);
	}
	
	@Test
	public void testIsHexMaxInt() {
		boolean isHex = UnicodeConverter.isUnicodeHexString("\\u7fffffff");
		assertFalse("Test max int hex returns false", isHex);
	}
	
	@Test
	public void testIsHexOutOfRange() {
		boolean isHex = UnicodeConverter.isUnicodeHexString("\\u80000000");
		assertFalse("Test out of range int hex returns false", isHex);
	}
	
	@Test
	public void testIsHexZero() {
		boolean isHex = UnicodeConverter.isUnicodeHexString("\\u0000");
		assertTrue("Test zero hex returns true ", isHex);
	}
	
	@Test
	public void testGetJavaUnciodeCharNegative() {
		String character = UnicodeConverter.getJavaUnicodeChar(-1);
		assertEquals("Test negative int returns an empty string", "", character);
	}
	
	@Test
	public void testGetJavaUnciodeCharMaxInt() {
		String character = UnicodeConverter.getJavaUnicodeChar(Integer.MAX_VALUE);
		assertEquals("Test max int returns an empty string", "", character);
	}
	
	@Test
	public void testGetJavaUnciodeCharMin() {
		String character = UnicodeConverter.getJavaUnicodeChar(Integer.MIN_VALUE);
		assertEquals("Test min int returns an empty string", "", character);
	}
	
	@Test
	public void testGetJavaUnciodeCharValidChar() {
		String character = UnicodeConverter.getJavaUnicodeChar(48);
		assertEquals("Test valid int returns the correct character", "0", character);
	}
	
	@Test
	public void testGetJavaUnciodeCharOutOfRange() {
		String character = UnicodeConverter.getJavaUnicodeChar(1114112);
		assertEquals("Test out of range int returns an empty string", "", character);
	}
	
	@Test
	public void testGetJavaUnciodeCharMaxIntHex() {
		String character = UnicodeConverter.getJavaUnicodeChar("\\u7fffffff");
		assertEquals("Test max int hex returns an empty string", "", character);
	}
	
	@Test
	public void testGetJavaUnciodeCharValidCharHex() {
		String character = UnicodeConverter.getJavaUnicodeChar("\\u0030");
		assertEquals("Test valid hex returns the correct character", "0", character);
	}
	
	@Test
	public void testGetJavaUnciodeCharOutOfUnicodeRangeHex() {
		String character = UnicodeConverter.getJavaUnicodeChar("\\u110000");
		assertEquals("Test out of range hex returns an empty string", "", character);
	}
	
	@Test
	public void testGetJavaUnciodeCharOutOfRangeHex() {
		String character = UnicodeConverter.getJavaUnicodeChar("\\u80000000");
		assertEquals("Test hex larger than max int returns an empty string", "", character);
	}
}
