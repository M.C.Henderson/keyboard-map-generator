package mapping;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class TreeNodeTests {
	
	KeyMap parent;
	KeyMap child;
	KeyMap sibling;
	TreeNode<KeyMap> childNode;
	TreeNode<KeyMap> parentNode;
	TreeNode<KeyMap> emptyNode;
	
	
	@BeforeEach
	public void setup() {
		child = new KeyMap("Child");
		parent = new KeyMap("Parent");
		sibling = new KeyMap("Sibling");
		
		parentNode = new TreeNode<KeyMap>(parent, null);
		childNode = new TreeNode<KeyMap>(child, parentNode);
		emptyNode = new TreeNode<KeyMap>(null, null);
		
		parentNode.addChild(childNode.getData());
		parentNode.addChild(sibling);
	}
	
	@Test
	public void testConstructor() {
		assertEquals("Test that the constructor sets the parent", parent, childNode.getParent().getData());
		assertEquals("Test that the constrcutor sets the data properly", child, childNode.getData());
		//Test null values in the constructor. This happens in the case of the root node
		assertEquals("Test that node without parent returns null for parent", null, parentNode.getParent());
		assertEquals("Test that node without data returns null for data", null, emptyNode.getData());
	}

	@Test
	public void testHasChildren() {
		assertTrue("Test that a node with children returns true", parentNode.hasChildren());
		assertFalse("Test that a node without children returns false", childNode.hasChildren());
	}

	@Test
	public void testRemoveChild() {
		assertFalse("Test that the method returns false if the child does not exist", parentNode.removeChild(new KeyMap("Test")));
		
		assertFalse("Test that the methods returns false if the keymap is null", parentNode.removeChild(null));
		
		assertTrue("Test that method returns true if the first child exists", parentNode.removeChild(sibling));
		testDoesNotContain(sibling, parentNode.getChildren(), "Test the first element has been removed");
		assertTrue("Test that parent still has a child", parentNode.hasChildren());
		
		assertTrue("Test that method returns true if the second child exists", parentNode.removeChild(child));
		testDoesNotContain(child, parentNode.getChildren(), "Test that the second element has been removed");
		assertFalse("Test that parent has no children", parentNode.hasChildren());
		
		assertFalse("Test that the method returns false when there is no children", parentNode.removeChild(new KeyMap("Test")));
	}
	
	@Test
	public void addChild() {
		KeyMap one = new KeyMap("one");
		KeyMap two = new KeyMap("two");
		KeyMap three = new KeyMap("three");
		childNode.addChild(one);
		childNode.addChild(two);
		childNode.addChild(three);
		assertTrue("Test that the children nodes have registered", childNode.hasChildren());
		List<TreeNode<KeyMap>> children = childNode.getChildren();
		
		testContains(one, children, "Test that all children elements have been added");
		testContains(two, children, "Test that all children elements have been added");
		testContains(three, children, "Test that all children elements have been added");
	}
	
	private void testDoesNotContain(KeyMap map, List<TreeNode<KeyMap>> children, String message) {
		boolean contains = false;
		for (TreeNode<KeyMap> child: children) {
			if (map.equals(child.getData())) {
				contains = true;
			}
		}
		assertFalse(message, contains);
	}
	
	private void testContains(KeyMap map, List<TreeNode<KeyMap>> children, String message) {
		boolean contains = false;
		for (TreeNode<KeyMap> child: children) {
			if (map.equals(child.getData())) {
				contains = true;
			}
		}
		assertTrue(message, contains);
	}
}
