package mapping;

import static org.junit.Assert.*;

import java.awt.image.BufferedImage;
import java.util.HashMap;

import org.junit.jupiter.api.Test;

class KeyMapTests {

	@Test
	public void testConstructor() {
		KeyMap keymap = new KeyMap("test");
		assertEquals("Test name is set in constructor", "test", keymap.getName());
	}
	
	@Test
	public void testSetName() {
		KeyMap keymap = new KeyMap("old");
		keymap.setName("new");
		assertEquals("Test set name method", "new", keymap.getName());
	}
	
	@Test
	public void testGetValidKeyLeft() {
		KeyMap keymap = new KeyMap("test");
		Key key = keymap.getKey("Ctrl left");
		assertEquals("Test valid key is retrived", "Ctrl left", key.getUniqueKeyIdentifier());
		assertEquals("Test valid key is retrived", "left", key.getLocation());
		assertEquals("Test valid key is retrived", "Ctrl", key.getKeyName());
		assertEquals("Test valid key is retrived", 17, key.getVkKeyCode());
	}
	
	@Test
	public void testGetValidKeyRight() {
		KeyMap keymap = new KeyMap("test");
		Key key = keymap.getKey("Alt right");
		assertEquals("Test valid key is retrived", "Alt right", key.getUniqueKeyIdentifier());
		assertEquals("Test valid key is retrived", "right", key.getLocation());
		assertEquals("Test valid key is retrived", "Alt", key.getKeyName());
		assertEquals("Test valid key is retrived", 18, key.getVkKeyCode());
	}
	
	@Test
	public void testGetValidKeyStandard() {
		KeyMap keymap = new KeyMap("test");
		Key key = keymap.getKey("C standard");
		assertEquals("Test valid key is retrived", "C standard", key.getUniqueKeyIdentifier());
		assertEquals("Test valid key is retrived", "standard", key.getLocation());
		assertEquals("Test valid key is retrived", "C", key.getKeyName());
		assertEquals("Test valid key is retrived", 67, key.getVkKeyCode());
	}
	
	@Test
	public void testGetValidKeyNumpad() {
		KeyMap keymap = new KeyMap("test");
		Key key = keymap.getKey("End numpad");
		assertEquals("Test valid key is retrived", "End numpad", key.getUniqueKeyIdentifier());
		assertEquals("Test valid key is retrived", "numpad", key.getLocation());
		assertEquals("Test valid key is retrived", "End", key.getKeyName());
		assertEquals("Test valid key is retrived", 35, key.getVkKeyCode());
	}
	
	@Test
	public void testGetInvalidKey() {
		KeyMap keymap = new KeyMap("test");
		Key key = keymap.getKey("invalid standard");
		assertEquals("Test that getting an invalid key returns null", null, key);
	}
	
	@Test
	public void testGetNullKey() {
		KeyMap keymap = new KeyMap("test");
		Key key = keymap.getKey(null);
		assertEquals("Test that getting an null key returns null", null, key);
	}
	
	@Test
	public void testGeneratedKeymap() {
		KeyMap keymap = new KeyMap("test");
		HashMap<String, Key> map = keymap.getMap();
		//There are 915 possible of combinations of key name and location
		//This method failing is a good indication that java have changed the number of supported keys
		assertEquals("Test the correct number of keys has been generated", 915, map.size());
	}
	
	@Test
	public void testCompareToSame() {
		KeyMap one = new KeyMap("same");
		KeyMap two = new KeyMap("same");
		assertEquals("Test compare to method returns 0 for keymaps with the same name", 0, one.compareTo(two));
	}
	
	@Test
	public void testCompareToDifferent() {
		KeyMap one = new KeyMap("same");
		KeyMap two = new KeyMap("different");
		assertNotEquals("Test compare to method does not return 0 for keymaps with different name", 0, one.compareTo(two));
	}
	
	@Test
	public void testToString() {
		KeyMap keymap = new KeyMap("test");
		assertEquals("Test toString method gets the name of the keymap", "test", keymap.toString());
	}
	
	@Test
	public void testResetMapping() {
		KeyMap keymap = new KeyMap("test");
		Key key = keymap.getKey("Ctrl left");
		key.setMapping("\\u0032");
		assertTrue("Test the mapping has been set", key.isMapped());
		keymap.reset();
		assertFalse("Test that the mapped key has been reset", key.isMapped());
	}
	
	@Test
	public void testResetRandomised() {
		KeyMap keymap = new KeyMap("test");
		Key key = keymap.getKey("Home numpad");
		key.randomise();
		assertTrue("Test the randomised key has been set", key.isRandomised());
		keymap.reset();
		assertFalse("Test that the randomised key has been reset", key.isRandomised());
		assertFalse("Test that the the text overlay has been reset", key.isRandomised());
	}
	
	@Test
	public void testResetText() {
		KeyMap keymap = new KeyMap("test");
		Key key = keymap.getKey("Alt right");
		key.setTextOverlay("overlay");
		key.setMapping("");
		assertTrue("Test the text overlay has been set", key.isTextOverlay());
		keymap.reset();
		assertEquals("Test that the text overlay has been reset", null, key.getTextOverlay());
		assertTrue("Test that the display type is default", key.isDefaultDisplay());
	}
	
	@Test
	public void testResetImage() {
		KeyMap keymap = new KeyMap("test");
		Key key = keymap.getKey("Alt right");
		key.setImage(new BufferedImage(1, 1, 1));
		assertTrue("Test the image has been set", key.isImageOverlay());
		key.setMapping("");
		keymap.reset();
		assertEquals("Test that the image has been reset", null, key.getImage());
		assertTrue("Test that the display type is default", key.isDefaultDisplay());
	}
}
