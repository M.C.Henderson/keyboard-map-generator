package mapping;

import static org.junit.Assert.*;

import java.awt.image.BufferedImage;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class KeyTests {
	
	private Key key;
	
	@BeforeEach
	public void setup() {
		key = new Key("P", "standard", 80);
	}

	@Test
	public void testConstructor() {
		assertEquals("Test key name is set by constructor", "P", key.getKeyName());
		assertEquals("Test key location is set by the constructor", "standard", key.getLocation());
		assertEquals("Test key code is set properly by the constrcutor", 80, key.getVkKeyCode());
		assertTrue("Test display is initialised to default", key.isDefaultDisplay());
		assertFalse("Test mapping is not initialised", key.isMapped());
		assertFalse("Test key in not randomised", key.isRandomised());
		assertFalse("Test key is not a trigger key", key.isTrigger());
		assertEquals("Test that the unique key identifier has been set correctly", "P standard", key.getUniqueKeyIdentifier());
	}
	
	@Test
	public void testSetMapping() {
		key.setMapping("\\u0032");
		assertEquals("Test mapping is return correctly", "\\u0032", key.getMappingUnicode());
		assertTrue("Test key is set to mapped", key.isMapped());
	}
	
	@Test
	public void testSetRandomised() {
		key.randomise();
		assertTrue("Test the key is set to randomised", key.isRandomised());
	}
	
	@Test
	public void testSetTrigger() {
		key.setAsTrigger();
		assertTrue("Test the key has been set as a trigger", key.isTrigger());
	}
	
	@Test
	public void testSetTextOverlay() {
		key.setTextOverlay("new text");
		assertEquals("Test that the text has been set correctly", "new text", key.getTextOverlay());
		assertTrue("Test that the display type has been set to text", key.isTextOverlay());
		assertFalse("Test that the display type is not image", key.isImageOverlay());
		assertFalse("Test that the display type is not default", key.isDefaultDisplay());
		assertEquals("Test the display enum is return correctly", Key.DisplayType.TEXT, key.getDisplayType());
	}
	
	@Test
	public void testSetImageOverlay() {
		BufferedImage img = new BufferedImage(1, 1, 1);
		key.setImage(img);
		assertEquals("Test that the image has been set correctly", img, key.getImage());
		assertFalse("Test that the display type is not text overlay", key.isTextOverlay());
		assertTrue("Test that the display type has been set to image", key.isImageOverlay());
		assertFalse("Test that the display type is not default", key.isDefaultDisplay());
		assertEquals("Test the display enum is return correctly", Key.DisplayType.IMAGE, key.getDisplayType());
	}
	
	@Test
	public void testRemoveTextDisplay() {
		key.setTextOverlay("new text");
		key.removeCustomDisplay();
		assertFalse("Test that the display type is not text overlay", key.isTextOverlay());
		assertEquals("Test that the display text has been removed", null, key.getTextOverlay());
		assertTrue("Test that the defualt display type has been restored", key.isDefaultDisplay());
		assertEquals("Test the display enum is return correctly", Key.DisplayType.DEFAULT, key.getDisplayType());
	}
	
	@Test
	public void testRemoveImageDisplay() {
		BufferedImage img = new BufferedImage(1, 1, 1);
		key.setImage(img);
		key.restoreToDefault();
		assertFalse("Test that the display type is not text overlay", key.isTextOverlay());
		assertEquals("Test that the display image has been removed", null, key.getImage());
		assertFalse("Test that the display type is not image", key.isImageOverlay());
		assertEquals("Test the display enum is return correctly", Key.DisplayType.DEFAULT, key.getDisplayType());
	}
	
	@Test
	public void testChangeDisplayTextToImage() {
		testSetTextOverlay();
		testSetImageOverlay();
	}
	
	@Test
	public void testChangeDisplayImageToText() {
		testSetImageOverlay();
		testSetTextOverlay();
	}
	
	@Test
	public void testRestoreToDefault() {
		key.setMapping("\\u0032");
		key.randomise();
		key.setAsTrigger();
		key.restoreToDefault();
		assertFalse("Test the mapping has been removed", key.isMapped());
		assertEquals("Test the mapping unicode has been removed", null, key.getMappingUnicode());
		assertFalse("Test trigger status has been removed", key.isTrigger());
		assertFalse("Test randomisation has been removed", key.isRandomised());
	}
}
