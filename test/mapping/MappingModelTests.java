package mapping;

import static org.junit.Assert.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class MappingModelTests {
	
	private MappingModel model;
	
	@BeforeEach
	public void setUp() {
		model = new MappingModel();
	}
	
	@Test
	public void testAddKeyMapAtRoot() {
		KeyMap keymap = model.createNewKeyMap("test", null);
		assertEquals("test root keymap is added to the model", keymap, model.getKeyMap("test"));
	}
	
	@Test
	public void testAddKeyMapAsChild() {
		model.createNewKeyMap("parent", null);
		KeyMap child = model.createNewKeyMap("child", "parent");
		assertEquals("test child keymap is added to the model", child, model.getKeyMap("child"));
	}
	
	@Test
	public void addDuplicatKeyMap() {
		model.createNewKeyMap("test", null);
		KeyMap keymap = model.createNewKeyMap("test", null);
		assertEquals("test duplicate keymap is not added", null, keymap);
	}
	
	@Test
	public void testKeyMapNameIsAvailable() {
		assertTrue("test the method returns true if name is available", model.isKeyMapNameAvailable("test"));
	}
	
	@Test
	public void testKeyMapNameIsNotAvailable() {
		model.createNewKeyMap("test", null);
		assertFalse("test the method returns false if name is not available", model.isKeyMapNameAvailable("test"));
	}
	
	@Test
	public void testIsEmpty() {
		assertTrue("test empty keymap returns true", model.isEmpty());
	}
	
	@Test
	public void testIsNotEmpty() {
		model.createNewKeyMap("test", null);
		assertFalse("test non-empty model returns false", model.isEmpty());
	}
	
	@Test
	public void testDeleteKeymap() {
		model.createNewKeyMap("test", null);
		model.deleteKeymap("test");
		assertTrue("test empty model returns true", model.isEmpty());
	}
	
	@Test
	public void testGetNonExistentKeyMap() {
		assertEquals("test method returns null if the key map is not in the model", null, model.getKeyMap("test"));
	}
	
	@Test
	public void testGetKeyValidKey() {
		model.createNewKeyMap("test", null);
		Key key = model.getKey("S standard", "test");
		assertEquals("test retrieved key name", (Object) "S", (Object) key.getKeyName());
		assertEquals("test retrieved key location", (Object) "standard", (Object) key.getLocation());
	}
	
	@Test
	public void testGetKeyFromNonExistentMap() {
		Key key = model.getKey("S standard", "test");
		assertEquals("test retrieving key from non existent map", null, key);
	}
	
	@Test
	public void testGetNonExistentKey() {
		model.createNewKeyMap("test", null);
		Key key = model.getKey("xyz standard", "test");
		assertEquals("test retrieving key from non existent map", null, key);
	}
	
	@Test
	public void testGetKeyMapTree() {
		KeyMap keymap = model.createNewKeyMap("test", null);
		TreeDataStructure<KeyMap> keymaps = model.getKeyMapTree();
		assertTrue("test correct tree map has been returned", keymaps.contains(keymap));
	}
}
