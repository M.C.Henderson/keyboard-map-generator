package mapping;

import org.junit.jupiter.api.BeforeEach;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.NoSuchElementException;

import org.junit.jupiter.api.Test;

class TreesDataStructureTests {
	
	private TreeDataStructure<KeyMap> tds;
	
	@BeforeEach
	private void setup() {
		tds = new TreeDataStructure<KeyMap>();
	}

	@Test
	public void TestAddTopLevelKeymaps() {
		KeyMap first = new KeyMap("first");
		assertTrue("Test that the method return is correct", tds.add(first, null));
		assertTrue("Test that a top level element is added to the structure", tds.contains(first));
		KeyMap second = new KeyMap("second");
		assertTrue("Test that the method return is correct", tds.add(second, null));
		assertTrue("Test that multiple top level elements are added to the structure", tds.contains(second));
	}

	@Test
	public void testAddChildKeymaps() {
		KeyMap parent = new KeyMap("parent");
		tds.add(parent, null);
		KeyMap child = new KeyMap("child");
		assertTrue("Test that the method return is correct", tds.add(child, parent));
		assertTrue("Test that child elements are added to the structure", tds.contains(child));
		KeyMap sibling = new KeyMap("sibling");
		assertTrue("Test that the method return is correct", tds.add(sibling, parent));
		assertTrue("Test that sibling child elements are added to the structure", tds.contains(sibling));
		KeyMap grandChild = new KeyMap("grandChild");
		assertTrue("Test that the method return is correct", tds.add(grandChild, child));
		assertTrue("Test that grandchild elements are added to the structure", tds.contains(grandChild));
	}
	
	@Test
	public void testAddWithIncorrectParent() {
		//Add to a keymap that isn't in the tree
		KeyMap parent = new KeyMap("parent");
		KeyMap child = new KeyMap("child");
		assertFalse("Test that the method return is correct", tds.add(child, parent));
		assertFalse("Test that child elements are added to the structure", tds.contains(child));
	}
	
	@Test
	public void testAddNull() {
		assertFalse("Test that adding null to the tree returns false", tds.add(null, null));
	}
	
	@Test
	public void testContainsNull() {
		assertFalse("Test that checking for null keymap returns false", tds.contains(null));
	}
	
	@Test
	public void testGetParent() {
		KeyMap parent = new KeyMap("parent");
		tds.add(parent, null);
		KeyMap child = new KeyMap("child");
		tds.add(child, parent);
		assertEquals("Test parent is correctly retrieved", parent, tds.getParent(child));
	}
	
	@Test
	public void testGetParentOfTopLevelElement() {
		KeyMap parent = new KeyMap("parent");
		tds.add(parent, null);
		assertEquals("Test that null is returned when the element does not have a parent", null, tds.getParent(parent));
	}
	
	@Test
	public void testGetParentOfElementNotInTree() {
		KeyMap test = new KeyMap("test");
		assertEquals("Test getting parent of element not in the map", null, tds.getParent(test));
	}
	
	@Test
	public void getParentOfNull() {
		assertEquals("Test that getting the parent of null returns null", null, tds.getParent(null));
	}
	
	@Test
	public void testTopLevelKeymapRemoval() {
		KeyMap test = new KeyMap("test");
		tds.add(test, null);
		tds.remove(test);
		assertFalse("Test that to level elements are removed from the tree", tds.contains(test));
	}

	@Test
	public void testChildKeymapRemoval() {
		KeyMap parent = new KeyMap("parent");
		tds.add(parent, null);
		KeyMap child = new KeyMap("child");
		tds.add(child, parent);
		tds.remove(child);
		assertFalse("Test that to level elements are removed from the tree", tds.contains(child));
	}
	
	@Test
	public void testNoOrphans() {
		KeyMap parent = new KeyMap("parent");
		tds.add(parent, null);
		KeyMap child = new KeyMap("child");
		tds.add(child, parent);
		tds.remove(parent);
		assertFalse("Test that the structure does not leave orphans", tds.contains(child));
	}
	
	@Test
	public void testRomoveNotInTree() {
		KeyMap test = new KeyMap("test");
		assertFalse("Test that removing a keymap not in the tree returns false", tds.remove(test));
	}
	
	@Test
	public void testRemoveNull() {
		assertFalse("Test that removing null to the tree returns false", tds.remove(null));
	}
	
	@Test
	public void testIsEmptyTrue() {
		assertTrue("Test that empty tree returns true", tds.isEmpty());
	}
	
	@Test
	public void testIsEmptyFalse() {
		KeyMap test = new KeyMap("test");
		tds.add(test, null);
		assertFalse("Test that non-empty tree returns false", tds.isEmpty());
	}
	
	@Test
	public void testPreOrderIterator() {
		KeyMap one = new KeyMap("one");
		KeyMap two = new KeyMap("two");
		KeyMap three = new KeyMap("three");
		KeyMap four = new KeyMap("four");
		KeyMap five = new KeyMap("five");
		KeyMap six = new KeyMap("six");
		KeyMap seven = new KeyMap("seven");
		KeyMap eight = new KeyMap("eight");
		tds.add(one, null);
		tds.add(two, null);
		tds.add(three, one);
		tds.add(four, one);
		tds.add(five, one);
		tds.add(six, two);
		tds.add(seven, two);
		tds.add(eight, six);
		
		LinkedList<KeyMap> order = new LinkedList<KeyMap>();
		order.add(one);
		order.add(two);
		order.add(three);
		order.add(four);
		order.add(five);
		order.add(six);
		order.add(seven);
		order.add(eight);
		
		Iterator<KeyMap> it = tds.iterator();
		
		int i = 0;
		while (it.hasNext()) {
			assertEquals("Test order of iterator is correct", order.get(i), it.next());
			i++;
		}
	}
	
	@Test
	public void testEmptyIterator() {
		Iterator<KeyMap> it = tds.iterator();
		assertThrows(NoSuchElementException.class, () -> it.next(), "Test empty iterator throws no such element exception");
	}
}
