## Augmented Keyboard Mapping Tool
This is an application to define augmented keyboard layouts for use in virtual reality. It allows you to define mappings and displays for keys. Multiple mappings can be built and can be layered on top of each other for complex results. An XML configuration file of these mappings can then be generated. Configuration files can also be loaded for editing. The XML produced is conformant to the schema which can be seen at [src/res](https://gitlab.com/M.C.Henderson/keyboard-map-generator/tree/master/res/dtd). When loading in configuration files, the XML is validated against the same schema.

## Motivation
This application was created in partial fulfilment of the requirements of the Degree of Master of Science at the University of Glasgow (Software Development).

## Glossary
* *Mapping* - The Unicode value that will be assigned to the keyboard key in question, overwriting the default input for the key
* *Key Map* - A group of key mappings, each with the same trigger key
* *Trigger Key* - The key on the keyboard that activates the key map when pressed
* *Unique Key Identifier* - Combination of key name and location that identifies the key within the application
* *Image Overlay* - The image that will be rendered on a keyboard key
* *Text Overlay* - The text that will be rendered on a keyboard key

## Screenshots
![Main GUI](https://gitlab.com/M.C.Henderson/keyboard-map-generator/blob/master/res/images/screenshots/screenshot_main_1.png) ![Main GUI](https://gitlab.com/M.C.Henderson/keyboard-map-generator/blob/master/res/images/screenshots/screenshot_main_2.png) ![New Keymap Window](https://gitlab.com/M.C.Henderson/keyboard-map-generator/blob/master/res/images/screenshots/screenshot_new_1.png) ![Advanced Mapping Selection](https://gitlab.com/M.C.Henderson/keyboard-map-generator/blob/master/res/images/screenshots/screenshot_advanced_1.png)

## Tech/framework used
This application was built with java swing. It was built as an Eclipse project. It utilises JUnit testing.

## Features
* Create key maps
* Layer key maps
* Assign mappings to keys
* Assign display content to keys
* Randomise groups of keys
* Generate XML configuration file
* Load and edit XML configuration files

## Installation
To install simply pull and import into eclipse as a project.

## Tests
A suite of JUnit tests was developed for repeatable testing of the software model. Tests were written for the Key, KeyMap, MappingModel, UnicodeConverter and TreeDataStructure classes. The testing performed followed the best practices as defined by [junit.org](https://junit.org/junit4/faq.html#best_1). This includes the practice of only writing tests for code that could reasonably break. For example, direct tests were not written for simple get and set methods.

## How to use?
To use simply run the code and follow prompts on the windows that appear.

## License
This software is unlicensed.

## Credits
This software was built with the help and advice of Professor Stephen Brewster and Dr Mark McGill.

## Further work
* *Images* - Currently images are output to a folder alongside the XML configuration file. Embedding these images as base64 encoded strings within the XML file would mean all the mapping information is contained in the same file.
* *Trigger Actions* - The programme and DTD could be extended to encompass more complex trigger actions, such as gaze based functions.
* *Expanded Mappings* - The programme and DTD could be extended to allow more complex mappings, such as macros and application launching.
* *DTD* - At the moment only a local version of the DTD is available for validation. This means that the file must be present in the location on the filesystem stated in the document. This means that at the moment the XML should be loaded by the same version of the application that created it. A public version of the DTD hosted on a server would solve this.
* *Mid-air interactions* - Support for mid-air action could be added.
* *Drag and drop components* - Support for a larger selection of drag and drop components.
