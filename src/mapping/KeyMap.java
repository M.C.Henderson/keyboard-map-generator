package mapping;

import java.awt.Button;
import java.awt.event.KeyEvent;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.HashMap;

/**
 * Class to represent groupings of mapped keys on the keyboard. Populates 
 * with all possible keys. These are taken from the java KeyEvent by using 
 * reflection. Each key is identified with the unique key identifier, this is 
 * a combination of name and location.
 * @see KeyEvent
 * @author Matthew Henderson - University of Glasgow Masters Project 2018
 *
 */
public class KeyMap implements Comparable<KeyMap> {
	
	private String name;
	private HashMap<String, Key> keys;
	
	/**
	 * Creates a KeyMap object that represents the set of mapped keys. 
	 * It is recommended to use a unique name, although this is not 
	 * enforced at class level.
	 * @param name	the name of the KeyMap
	 */
	public KeyMap(String name) {
		this.name = name;
		initKeys();
	}
	
	/**
	 * Sets the name of the KeyMap.
	 * @param name	the name of the KeyMap
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Returns the name of the KeyMap.
	 * @return 	the name of the KeyMap
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Returns the Key object that represents the specified key.
	 * @param keyText	the name of the key
	 * @return			the requested key object
	 */
	public Key getKey(String keyText) {
		return keys.get(keyText);
	}
	
	/**
	 * Returns the HashMap that pairs key names to Key objects. 
	 * @return	the HashMap of keys associated with the KeyMap object
	 */
	public HashMap<String, Key> getMap() {
		return keys;
	}
	
	/**
	 * Compares two KeyMaps. This simply compares the names of the KeyMaps. 
	 */
	public int compareTo(KeyMap other) {
		return name.compareTo(other.getName());
	}
	
	/**
	 * Initialise the HashMap that pairs key identifiers to Key objects. This method creates a fake KeyEvent 
	 * and then uses reflection to find all the fields associated with the the KeyEvent object. This is necessary 
	 * because java stores all possible keyboard keys as static instance variables. These key events are liable 
	 * to update so dynamically creating this map from the native java class will help to keep the map up to date. 
	 * The name and location that java uses to identify each key button with is then found and used as the key in the map. 
	 * It is paired with a key object created from information found in the KeyEvent class.
	 */
	private void initKeys() {
		keys = new HashMap<String, Key>();
		
		//Create a fake key event
		Button a = new Button("click");
	    KeyEvent e;
	    e = new KeyEvent(a, 1, 20, 1, 10, 'a');
		
	    //Use reflection to find all keys supported by java KeyEvent
		Field[] fields = e.getClass().getDeclaredFields();
		//Loop through all field in the key event call
		for (Field f : fields) {
	        try {
	        	int mod = f.getModifiers();
	        	//Keys are public static final int
	        	if (f.getType().toString().equals("int") && Modifier.toString(mod).equals("public static final")) {
	        		//Get the key text of the integer variable
	        		String keyText = KeyEvent.getKeyText(f.getInt(e));
		        	if (keyText.length() >= 15 && keyText.substring(0, 15).equals("Unknown keyCode")) {
		        		//Unknown keycode, don't add to map
		        	} else {
		        		int keyCode = f.getInt(e);
		        		
		        		//Put each key in array for each location
		        		//More than needed for most keyboards but gives contingency
		        		keys.put(keyText + " right", new Key(keyText, "right", keyCode));
		        		keys.put(keyText + " left", new Key(keyText, "left", keyCode));
		        		keys.put(keyText + " numpad", new Key(keyText, "numpad", keyCode));
		        		keys.put(keyText + " unknown", new Key(keyText, "unknown", keyCode));
		        		keys.put(keyText + " standard", new Key(keyText, "standard", keyCode));
		        	}
	        	}	
			} catch (IllegalArgumentException | IllegalAccessException e1) {
				//Catch exceptions associated with reflection and print to console for debugging
				e1.printStackTrace();
			}
		}
	}
	
	/**
	 * Restores all keys to their default display. This method removes all mapping 
	 * etc from every key in the keymap.
	 */
	public void reset() {
		for (Key key : keys.values()) {
		    if (key.isMapped() || key.isRandomised()) {
		    	key.restoreToDefault();
		    }
		}
	}
	
	/**
	 * Returns the String representation of the KeyMap object. 
	 * This is just the name of the KeyMap
	 */
	public String toString() {
		return name;
	}
}
