package mapping;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.imageio.ImageIO;
import javax.swing.JOptionPane;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

/**
 * Class to parse a pre-existing XML mappings file, these mappings can then be displayed the
 * user for editing. The XML file will be compared against the document type definition file 
 * defined for the mappings XML. This dtd file can be found in the resources folder or in the 
 * project report.
 * @author Matthew Henderson - University of Glasgow Masters Project 2018
 */
public class XmlParser {
	
	private String path;
	private MappingModel model;
	
	/**
	 * Class constructor
	 * @param path	the path to the XML file to be parsed
	 * @param model	the software model to be updated with information from the XML file
	 */
	public XmlParser(String path, MappingModel model) {
		this.path = path;
		this.model = model;
	}

	/**
	 * Method to parse the xml file specified in the constructor and to load the data 
	 * to the model.
	 */
	public void parse() {
		try {
			//Build a file from the path specified by the user
			File fXmlFile = new File(path);
			//Build the document object used to retrieve the elements of the XML
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			dbFactory.setValidating(true);
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			dBuilder.setErrorHandler(new ErrorHandler() {
			    @Override
			    public void error(SAXParseException exception) {
			        //error, display message
			    	JOptionPane.showMessageDialog(null, "There was a problem whilst validating the XML file. "
			    			+ "Error: \n." + exception.toString(), "error", JOptionPane.PLAIN_MESSAGE);
			    }
			    @Override
			    public void fatalError(SAXParseException exception) throws SAXException {
			    	//error, display message
			    	JOptionPane.showMessageDialog(null, "Fatal error whilst validating the XML. "
			    			+ "Please ensure that the file conforms to the mapping schema" + exception.toString(), "fatal error", JOptionPane.PLAIN_MESSAGE);
			    	//Close the program in the case of a fatal error
			    	System.exit(0);
			    }

			    @Override
			    public void warning(SAXParseException exception) {
			    	//error, display message
			    	JOptionPane.showMessageDialog(null, "There was a problem whilst the XML file. "
			    			+ "Please see the below error message: \n." + exception.toString(), "warning", JOptionPane.PLAIN_MESSAGE);
			    }
			});
			
			//Parse and normalise document
			Document doc = dBuilder.parse(fXmlFile);
			doc.getDocumentElement().normalize();
			//Create a node list of all the keymap elements
			NodeList nList = doc.getElementsByTagName("keymap");
			
			for (int i = 0; i < nList.getLength(); i++) {
				//Loop through the node list to parse each keymap element
				Node nNode = nList.item(i);
				
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					//If the node is an element node, cast it to an element
                    Element keymapElement = (Element) nNode;
                    //Get the nodes parent and cast to an element
                    Element parent = (Element) keymapElement.getParentNode();
                    //Parse the element
                    parseKeymap(keymapElement, parent);
                }
			}
		//Give appropriate error messages 
		} catch (ParserConfigurationException e) {
			JOptionPane.showMessageDialog(null, "Problem with parser congfiguration. Please ensure"
					+ "you have a chosen a valid file.", "error", JOptionPane.PLAIN_MESSAGE);
		} catch (SAXException e) {
			JOptionPane.showMessageDialog(null, "SAX error. This could be a proplem with validation. "
					+ "Please ensure the XML matches the shema.", "error", JOptionPane.PLAIN_MESSAGE);
		} catch (IOException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, "IO error. "
					+ "Please ensure you have chosen a valid file.", "error", JOptionPane.PLAIN_MESSAGE);
		}
	}

	/**
	 * Helper method to parse a keymap element from the XML file. This method first checks 
	 * if the keymap's parent is another keymap element and then builds the KeyMap object in 
	 * the model. If yes, the keymap is added in the correct place in the hierarchy. If no 
	 * the keymap is added to the root of the tree. After this all child elements are parsed. 
	 * This allows mapped keys and trigger keys to be added to the model, as well as any special 
	 * display content.
	 * @param element	the keymap element to be parsed
	 * @param parent	the parent element of the keymap element to be parsed
	 */
	private void parseKeymap(Element element, Element parent) {
		//Create a NamedNodeMap containing all the keymaps attributes
		NamedNodeMap nnm = element.getAttributes();
		//Get the name of the keymap
        String name = nnm.getNamedItem("name").getTextContent();
        
        KeyMap keymap;
        
        if (parent.getNodeName().equals("mappings")) {
        	//Top level element has no keymap parent. Create keymap at top of hierarchy
        	keymap = model.createNewKeyMap(name, null);
        } else {
        	//Not a top level element, get the parent keymap name and create a KeyMap object in the model
        	NamedNodeMap parentNnm = parent.getAttributes();
        	String parentName = parentNnm.getNamedItem("name").getTextContent();
        	keymap = model.createNewKeyMap(name, parentName);
        }
        
        //Get the children of the keymap element and parse them
        NodeList nl = element.getChildNodes();
        parseKeymapChildren(keymap, nl);
	}
	
	/**
	 * Parse key and trigger elements of the XML.
	 * @param keymap	the keymap object that will be populated with data
	 * @param nl		the list of key and trigger nodes
	 */
	private void parseKeymapChildren(KeyMap keymap, NodeList nl) {
		for(int i = 0; i < nl.getLength(); i++) {
        	//Loop through all the child elements
            Node keyNode = nl.item(i);
            Key key;
            //Only parse key and trigger elements here. Child keymaps will be parsed later
            if(keyNode.getNodeName().equals("key") || keyNode.getNodeName().equals("trigger")) {
            	//Get all the attribute associated with both key and trigger elements
            	NamedNodeMap keyNnm = keyNode.getAttributes();
	            String keyName = keyNnm.getNamedItem("name").getTextContent();
	        	String location = keyNnm.getNamedItem("location").getTextContent();
	        	//In the model, keys are identified by the unique combination of key name and location
	        	String keyIdentifier = keyName + " " + location;
	        	key = keymap.getKey(keyIdentifier);
	        	
	        	//Parse key elements
	        	if (keyNode.getNodeName().equals("key")) {
	        		parseKeyElement(key, keyNode,  keyNnm);
	            //Parse the trigger elements
	            } else if (keyNode.getNodeName().equals("trigger")) {
	            	parseTriggerElement(key, keyNode);
	            }
            }    
        }
	}
	
	/**
	 * Parses a key element.
	 * @param key		the key object to be populated with data
	 * @param keyNode	the node of the XML element
	 * @param keyNnm	the map of attributes belonging to the node
	 */
	private void parseKeyElement(Key key, Node keyNode, NamedNodeMap keyNnm) {
		//Set whether the key is randomised
		String randomised = keyNnm.getNamedItem("randomised").getTextContent();
		if (randomised.equals("true")) {
			key.randomise();
		}
		
		//Keep track of whether the key is mapped
		boolean isMapped = false;
		
    	NodeList keyNl = keyNode.getChildNodes();
    	
    	for(int j = 0; j < keyNl.getLength(); j++) {
    		//Loop through all children of the Key element
    		Node displayNode  = keyNl.item(j);
    		
    		//Set the key mapping
    		if (displayNode.getNodeName().equals("mapping")) {
    			String mapping = displayNode.getTextContent();
    			key.setMapping(mapping);
    			//Mark the key as mapped
    			isMapped = true;
    		}
    		
    		//Parse the display content
    		setDisplayContent(key, displayNode);
    	}
    	
    	//If the key is not mapped to Unicode. It is a display only element
    	if(!isMapped && randomised.equals("false")) {
    		key.setMapping("");
    	}
	}
	
	/**
	 * Parse a trigger element.
	 * @param key		the key object to be populated with data
	 * @param keyNode	the node of the XML element
	 */
	private void parseTriggerElement(Key key, Node keyNode) {
		//Set the key as a trigger
		key.setAsTrigger();
    	
		//Loop through children of the trigger element
    	NodeList triggerNl = keyNode.getChildNodes();
    	for(int j = 0; j < triggerNl.getLength(); j++) {
    		//Child elements relate to the display
    		Node displayNode  = triggerNl.item(j);
    		//Parse the display element
    		setDisplayContent(key, displayNode);
    	}
	}
	
	/**
	 * Set the display content of key objects.
	 * @param key			the key object to set the display of
	 * @param displayNode	the node containing the XML display data
	 */
	private void setDisplayContent(Key key, Node displayNode) {
		//The XML specifies a text overlay
		if (displayNode.getNodeName().equals("text")) {
			//Set the text overlay of the key
			String text = displayNode.getTextContent();
			key.setTextOverlay(text);
		//The XML specifies an image overlay
		} else if (displayNode.getNodeName().equals("img")) {
			//Get the path of the image stored in XML
			NamedNodeMap displayNnm = displayNode.getAttributes();
			String imgPath = displayNnm.getNamedItem("src").getTextContent();
			
			//Create a path object for the path of the XML mapping file
			Path xmlPath = Paths.get(path);
			//Get the string path of the parent directory of the XML mapping file
			String directory = xmlPath.getParent().toString();
			
			//The image will be stored in the image path relative to the 
			//parent directory of the mapping file
			File imgFile = new File(directory + "\\" + imgPath);
			BufferedImage img;
			
			//Create the image and set the key display
			try {
				img = ImageIO.read(imgFile);
				key.setImage(img);
			} catch (IOException e) {
				//The image has not been loaded correctly
				int n = JOptionPane.showConfirmDialog(null, "Could not load image. "
						+ "Please ensure that the generated image directory is "
						+ "in the same directory as the XML. \nWould you like to "
						+ "continue without images?", "error", JOptionPane.YES_NO_OPTION);
				//If the user doesn't want to continue without images 
				//Quit the application
				if (n==1) {
					System.exit(0);
				}
			}
		}
	}
}
