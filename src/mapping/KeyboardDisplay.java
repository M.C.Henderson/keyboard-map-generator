package mapping;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.HashMap;

import javax.swing.JButton;
import javax.swing.JPanel;

/**
 * The class where the virtual keyboard is defined. This class extends MainGUI and 
 * is intended to abstract the virtual keyboard away from the rest of the interface 
 * so that it can be easily replaced or modified. If creating a new keyboard display 
 * 3 rules must be followed. Firstly the keyboard must be built of JButtons and be 
 * added to the KeyboardArea JPanel. Secondly the buttons must be added to the HashMap 
 * called button key pairings. This HashMap matches the buttons to the unique identifier 
 * of the key that they represent. Thirdly the method buildKeyboard must be called.
 * @author Matthew Henderson - University of Glasgow Masters Project 2018
 *
 */
public class KeyboardDisplay extends MainGUI {
	//Arrays to represent the rows of keys on the keyboard
	private JButton[] firstRow = new JButton[16];
	private JButton[] secondRow = new JButton[21];
	private JButton[] thirdRow = new JButton[20];
	private JButton[] fourthRow = new JButton[16];
	private JButton[] fifthRow = new JButton[16];
	private JButton[] sixthRow = new JButton[13];

	//Components
	private JButton esc, f1, f2, f3, f4, f5, f6, f7, f8, f9, f10, f11, f12, ps, sLock, pause;
	private JButton bt, one, two, three, four, five, six, seven, eight, nine, zero, minus, eq, bs, insert, home, pUp, nLock, npSlash, star, npMinus;
	private JButton tab, q, w, e, r, t, y, u, i, o, p, lb, rb, bSlash, delete, end, pDown, npSeven, npEight, npNine;
	private JButton caps, a, s, d, f, g, h, j, k, l, col, apos, enter, npFour, npFive, npSix;
	private JButton lShift, z, x, c, v, b, n, m, comma, stop, slash, rShift, up, npOne, npTwo, npThree;
	private JButton lCtrl, lWin, lAlt, space, rAlt, rWin, rClick, left, down, right, rCtrl, npZero, npStop;
	private JButton npPlus, npEnter;
	private JPanel keyboard, keyboardFirst, keyboardSecond, keyboardThird, keyboardFourth, keyboardFifth, keyboardSixth;
	private GridBagConstraints keyConstraints, keyboardConstraints;

	/**
	 * Constructs the Keyboard display object.
	 * @param presenter	the presenter in the mvp architecture
	 * @param keymap	the keymap that will initially by selected
	 */
	public KeyboardDisplay(Presenter presenter, String keymap) {
		super(presenter, keymap);
		initButtonKeyPairings();
		initKeyboard();
		buildKeyboard();
	}

	/**
	 * This method organises the keyboard display and then adds it to
	 * the keyboardArea JPanel. The layout chosen for this application 
	 * is the American version of the logitech AR keyboard.
	 */
	private void initKeyboard() {
		//Initiate the JPanels which the keyboard will be built from
		//Each row of keys is seperate JPanel
		keyboard = new JPanel(new GridBagLayout());
		keyboardFirst = new JPanel(new GridBagLayout());
		keyboardSecond = new JPanel(new GridBagLayout());
		keyboardThird = new JPanel(new GridBagLayout());
		keyboardFourth = new JPanel(new GridBagLayout());
		keyboardFifth = new JPanel(new GridBagLayout());
		keyboardSixth = new JPanel(new GridBagLayout());
		keyboardConstraints = new GridBagConstraints();
		keyConstraints = new GridBagConstraints();

		//Organise each row of keys
		keyboardConstraints.gridx = 1;
		keyboardConstraints.gridy = 1;
		keyboardConstraints.gridwidth = 3;
		keyboard.add(keyboardFirst, keyboardConstraints);
		keyboardConstraints.gridy = 2;
		keyboard.add(keyboardSecond, keyboardConstraints);
		keyboardConstraints.gridy = 3;
		keyboardConstraints.gridwidth = 2;
		keyboard.add(keyboardThird, keyboardConstraints);
		keyboardConstraints.gridy = 4;
		keyboard.add(keyboardFourth, keyboardConstraints);
		keyboardConstraints.gridy = 5;
		keyboard.add(keyboardFifth, keyboardConstraints);
		keyboardConstraints.gridy = 6;
		keyboard.add(keyboardSixth, keyboardConstraints);

		keyboardConstraints.insets = new Insets(0, 0, 10, 10);
		//Add the two keys which extend multiple rows
		//These are the numberpad plus and enter buttons
		npPlus.setPreferredSize(new Dimension(60, 130));
		npPlus.setBackground(new Color(47, 79, 79));
		npPlus.setForeground(Color.WHITE);
		keyboardConstraints.insets = new Insets(0, 0, 10, 10);
		npEnter.setPreferredSize(new Dimension(60, 130));
		npEnter.setBackground(new Color(47, 79, 79));
		npEnter.setForeground(Color.WHITE);
		keyboardConstraints.gridx = 3;
		keyboardConstraints.gridy = 3;
		keyboardConstraints.gridwidth = 1;
		keyboardConstraints.gridheight = 2;
		keyboard.add(npPlus, keyboardConstraints);
		keyboardConstraints.gridy = 5;
		keyboard.add(npEnter, keyboardConstraints);

		//Add all the other buttons
		addFirstRow();
		addSecondRow();
		addThirdRow();
		addFourthRow();
		addFifthRow();
		addSixthRow();
		
		//Add the keyboard to the keyboardArea JPanel
		keyboardArea.add(keyboard);
	}

	/**
	 * Adds the first row of buttons to the display.
	 */
	private void addFirstRow() {
		//Reset x coordinate of the grid constraints
		keyConstraints.gridx = 1;

		//Add the first row of keys
		for (int i = 0; i< firstRow.length; i++) {
			//Set the margins
			if (i == 0 || i == 4 || i == 8) {
				keyConstraints.insets = new Insets(0, 0, 30, 53);
			} else if (i == 12) {
				keyConstraints.insets = new Insets(0, 0, 30, 40);
			} else if(i == 15) {
				keyConstraints.insets = new Insets(0, 0, 30, 320);
			}
			else {
				keyConstraints.insets = new Insets(0, 0, 30, 10);
			}
			//Set size and display options then add the button
			firstRow[i].setPreferredSize(new Dimension(60, 60));
			keyboardFirst.add(firstRow[i], keyConstraints);
			keyConstraints.gridx ++;
			firstRow[i].setBackground(new Color(47, 79, 79));
			firstRow[i].setForeground(Color.WHITE);
		}
	}

	/**
	 * Adds the second row of buttons to the display.
	 */
	private void addSecondRow() {
		//Reset x coordinate of the grid constraints
		keyConstraints.gridx = 1;

		//Add the second row of keys
		for (int i = 0; i< secondRow.length; i++) {
			//Set the margins and sizes
			if (i == 13) {
				secondRow[i].setPreferredSize(new Dimension(120, 60));	
				keyConstraints.insets = new Insets(0, 0, 10, 40);
			} else if (i == 16) {
				secondRow[i].setPreferredSize(new Dimension(60, 60));
				keyConstraints.insets = new Insets(0, 0, 10, 40);
			}
			else {
				secondRow[i].setPreferredSize(new Dimension(60, 60));
				keyConstraints.insets = new Insets(0, 0, 10, 10);
			}
			//Set the display options then add the button
			keyboardSecond.add(secondRow[i], keyConstraints);
			keyConstraints.gridx ++;
			secondRow[i].setBackground(new Color(47, 79, 79));
			secondRow[i].setForeground(Color.WHITE);
		}
	}

	/**
	 * Adds the third row of buttons to the display.
	 */
	private void addThirdRow() {
		//Reset x coordinate of the grid constraints
		keyConstraints.gridx = 1;

		//Add the third row of keys
		for (int i = 0; i< thirdRow.length; i++) {
			//Set the margins and sizes
			if (i == 0) {
				thirdRow[i].setPreferredSize(new Dimension(90, 60));
				keyConstraints.insets = new Insets(0, 0, 10, 10);
			} else if(i == 13) {
				thirdRow[i].setPreferredSize(new Dimension(90, 60));
				keyConstraints.insets = new Insets(0, 0, 10, 40);
			} else if (i == 16) {
				thirdRow[i].setPreferredSize(new Dimension(60, 60));
				keyConstraints.insets = new Insets(0, 0, 10, 40);
			} else {
				thirdRow[i].setPreferredSize(new Dimension(60, 60));
				keyConstraints.insets = new Insets(0, 0, 10, 10);
			}

			//Set the display options then add the button
			keyboardThird.add(thirdRow[i], keyConstraints);
			keyConstraints.gridx ++;
			thirdRow[i].setBackground(new Color(47, 79, 79));
			thirdRow[i].setForeground(Color.WHITE);
		}
	}

	/**
	 * Adds the fourth row of buttons to the display.
	 */
	private void addFourthRow() {
		//Reset x coordinate of the grid constraints
		keyConstraints.gridx = 1;

		//Add the fourth row of keys
		for (int i = 0; i< fourthRow.length; i++) {
			//Set the margins and sizes
			if (i == 0) {
				fourthRow[i].setPreferredSize(new Dimension(125, 60));
				keyConstraints.insets = new Insets(0, 0, 10, 10);
			} else if (i == 12) {
				fourthRow[i].setPreferredSize(new Dimension(125, 60));
				keyConstraints.insets = new Insets(0, 0, 10, 280);
			} else {
				fourthRow[i].setPreferredSize(new Dimension(60, 60));
				keyConstraints.insets = new Insets(0, 0, 10, 10);
			}

			//Set the display options then add the button
			keyboardFourth.add(fourthRow[i], keyConstraints);
			keyConstraints.gridx ++;
			fourthRow[i].setBackground(new Color(47, 79, 79));
			fourthRow[i].setForeground(Color.WHITE);
		}
	}

	/**
	 * Adds the fifth row of buttons to the display.
	 */
	private void addFifthRow() {
		//Reset x coordinate of the grid constraints
		keyConstraints.gridx = 1;

		//Add the fifth row of keys
		for (int i = 0; i< fifthRow.length; i++) {
			//Set the margins and sizes
			if (i == 0) {
				fifthRow[i].setPreferredSize(new Dimension(160, 60));
				keyConstraints.insets = new Insets(0, 0, 10, 10);
			} else if(i == 11) {
				fifthRow[i].setPreferredSize(new Dimension(160, 60));
				keyConstraints.insets = new Insets(0, 0, 10, 110);
			} else if(i == 12) {
				fifthRow[i].setPreferredSize(new Dimension(60, 60));
				keyConstraints.insets = new Insets(0, 0, 10, 110);
			} else {
				fifthRow[i].setPreferredSize(new Dimension(60, 60));
				keyConstraints.insets = new Insets(0, 0, 10, 10);
			}

			//Set the display options then add the button
			keyboardFifth.add(fifthRow[i], keyConstraints);
			keyConstraints.gridx ++;
			fifthRow[i].setBackground(new Color(47, 79, 79));
			fifthRow[i].setForeground(Color.WHITE);
		}
	}

	/**
	 * Adds the sixth row of buttons to the display.
	 */
	private void addSixthRow() {
		//Reset x coordinate of the grid constraints
		keyConstraints.gridx = 1;

		//Add the sixth row of keys
		for (int i = 0; i< sixthRow.length; i++) {
			//Set the margins and sizes
			if (i == 0) {
				sixthRow[i].setPreferredSize(new Dimension(90, 60));
				keyConstraints.insets = new Insets(0, 0, 10, 10);
			} else if(i == 3) {
				sixthRow[i].setPreferredSize(new Dimension(380, 60));
				keyConstraints.insets = new Insets(0, 0, 10, 10);
			} else if(i == 7) {
				sixthRow[i].setPreferredSize(new Dimension(90, 60));
				keyConstraints.insets = new Insets(0, 0, 10, 40);
			} else if (i == 8 || i == 9 || i == 12) {
				keyConstraints.gridwidth = 1;
				sixthRow[i].setPreferredSize(new Dimension(60, 60));
				keyConstraints.insets = new Insets(0, 0, 10, 10);
			} else if (i == 10) {
				sixthRow[i].setPreferredSize(new Dimension(60, 60));
				keyConstraints.insets = new Insets(0, 0, 10, 40);
			} else if (i == 11) {
				keyConstraints.gridwidth = 2;
				sixthRow[i].setPreferredSize(new Dimension(130, 60));
				keyConstraints.insets = new Insets(0, 0, 10, 10);
			} else {
				sixthRow[i].setPreferredSize(new Dimension(80, 60));
				keyConstraints.insets = new Insets(0, 0, 10, 10);
			}

			//Set the display options then add the button
			keyboardSixth.add(sixthRow[i], keyConstraints);
			keyConstraints.gridx ++;	
			if (i == 11) {
				keyConstraints.gridx ++;
			}
			sixthRow[i].setBackground(new Color(47, 79, 79));
			sixthRow[i].setForeground(Color.WHITE);
		}
	}

	/**
	 * Create the buttons and add them to the keyButtonsPairings HashMap. This 
	 * HashMap matches the buttons to the unique identifier of the key that they 
	 * represent. This method also adds the buttons to an array that is used for
	 * display purposes. 
	 */
	private void initButtonKeyPairings() {
		//Instantiate the hash map
		buttonKeyPairings = new HashMap<JButton, String>();

		//Create the buttons and put them in the hash map
		esc = new JButton("Esc");
		firstRow[0] = esc;
		buttonKeyPairings.put(esc, "Escape standard");
		f1 = new JButton("F1");
		firstRow[1] = f1;
		buttonKeyPairings.put(f1, "F1 standard");
		f2 = new JButton("F2");
		firstRow[2] = f2;
		buttonKeyPairings.put(f2, "F2 standard");
		f3 = new JButton("F3");
		firstRow[3] = f3;
		buttonKeyPairings.put(f3, "F3 standard");
		f4 = new JButton("F4");
		firstRow[4] = f4;
		buttonKeyPairings.put(f4, "F4 standard");
		f5 = new JButton("F5");
		firstRow[5] = f5;
		buttonKeyPairings.put(f5, "F5 standard");
		f6 = new JButton("F6");
		firstRow[6] = f6;
		buttonKeyPairings.put(f6, "F6 standard");
		f7 = new JButton("F7");
		firstRow[7] = f7;
		buttonKeyPairings.put(f7, "F7 standard");
		f8 = new JButton("F8");
		firstRow[8] = f8;
		buttonKeyPairings.put(f8, "F8 standard");
		f9 = new JButton("F9");
		firstRow[9] = f9;
		buttonKeyPairings.put(f9, "F9 standard");
		f10 = new JButton("F10");
		firstRow[10] = f10;
		buttonKeyPairings.put(f10, "F10 standard");
		f11 = new JButton("F11");
		firstRow[11] = f11;
		buttonKeyPairings.put(f11, "F11 standard");
		f12 = new JButton("F12");
		firstRow[12] = f12;
		buttonKeyPairings.put(f12, "F12 standard");
		ps = new JButton("Prt Sc");
		firstRow[13] = ps;
		buttonKeyPairings.put(ps, "Print Screen standard");
		sLock = new JButton("Scr Lk");
		firstRow[14] = sLock;
		buttonKeyPairings.put(sLock, "Scroll Lock standard");
		pause = new JButton("Pause");
		firstRow[15] = pause;
		buttonKeyPairings.put(pause, "Pause standard");

		bt = new JButton("`");
		secondRow[0] = bt;
		buttonKeyPairings.put(bt, "Back Quote standard");
		one = new JButton("1");
		secondRow[1] = one;
		buttonKeyPairings.put(one, "1 standard");
		two = new JButton("2");
		secondRow[2] = two;
		buttonKeyPairings.put(two, "2 standard");
		three = new JButton("3");
		secondRow[3] = three;
		buttonKeyPairings.put(three, "3 standard");
		four = new JButton("4");
		secondRow[4] = four;
		buttonKeyPairings.put(four, "4 standard");
		five = new JButton("5");
		secondRow[5] = five;
		buttonKeyPairings.put(five, "5 standard");
		six = new JButton("6");
		secondRow[6] = six;
		buttonKeyPairings.put(six, "6 standard");
		seven = new JButton("7");
		secondRow[7] = seven;
		buttonKeyPairings.put(seven, "7 standard");
		eight = new JButton("8");
		secondRow[8] = eight;
		buttonKeyPairings.put(eight, "8 standard");
		nine = new JButton("9");
		secondRow[9] = nine;
		buttonKeyPairings.put(nine, "9 standard");
		zero = new JButton("0");
		secondRow[10] = zero;
		buttonKeyPairings.put(zero, "0 standard");
		minus = new JButton("-");
		secondRow[11] = minus;
		buttonKeyPairings.put(minus, "Minus standard");
		eq = new JButton("+");
		secondRow[12] = eq;
		buttonKeyPairings.put(eq, "Equals standard");
		bs = new JButton("Backspace");
		secondRow[13] = bs;
		buttonKeyPairings.put(bs, "Backspace standard");
		insert = new JButton("Ins");
		secondRow[14] = insert;
		buttonKeyPairings.put(insert, "Insert standard");
		home = new JButton("Home");
		secondRow[15] = home;
		buttonKeyPairings.put(home, "Home standard");
		pUp = new JButton("Pg Up");
		secondRow[16] = pUp;
		buttonKeyPairings.put(pUp, "Page Up standard");
		nLock = new JButton("Num Lk");
		secondRow[17] = nLock;
		buttonKeyPairings.put(nLock, "Num Lock numpad");
		npSlash = new JButton("/");
		secondRow[18] = npSlash;
		buttonKeyPairings.put(npSlash, "NumPad / numpad");
		star = new JButton("*");
		secondRow[19] = star;
		buttonKeyPairings.put(star, "NumPad * numpad");
		npMinus = new JButton("-");
		secondRow[20] = npMinus;
		buttonKeyPairings.put(npMinus, "NumPad - numpad");

		tab = new JButton("Tab");
		thirdRow[0] = tab;
		buttonKeyPairings.put(tab, "Tab standard");
		q = new JButton("Q");
		thirdRow[1] = q;
		buttonKeyPairings.put(q, "Q standard");
		w = new JButton("W");
		thirdRow[2] = w;
		buttonKeyPairings.put(w, "W standard");
		e = new JButton("E");
		thirdRow[3] = e;
		buttonKeyPairings.put(e, "E standard");
		r = new JButton("R");
		thirdRow[4] = r;
		buttonKeyPairings.put(r, "R standard");
		t = new JButton("T");
		thirdRow[5] = t;
		buttonKeyPairings.put(t, "T standard");
		y = new JButton("Y");
		thirdRow[6] = y;
		buttonKeyPairings.put(y, "Y standard");
		u = new JButton("U");
		thirdRow[7] = u;
		buttonKeyPairings.put(u, "U standard");
		i = new JButton("I");
		thirdRow[8] = i;
		buttonKeyPairings.put(i, "I standard");
		o = new JButton("O");
		thirdRow[9] = o;
		buttonKeyPairings.put(o, "O standard");
		p = new JButton("P");
		thirdRow[10] = p;
		buttonKeyPairings.put(p, "P standard");
		lb = new JButton("[");
		thirdRow[11] = lb;
		buttonKeyPairings.put(lb, "Open Bracket standard");
		rb = new JButton("]");
		thirdRow[12] = rb;
		buttonKeyPairings.put(rb, "Close Bracket standard");
		bSlash = new JButton("\\");
		thirdRow[13] = bSlash;
		buttonKeyPairings.put(bSlash, "Back Slash standard");
		delete = new JButton("Del");
		thirdRow[14] = delete;
		buttonKeyPairings.put(delete, "Delete standard");
		end = new JButton("End");
		thirdRow[15] = end;
		buttonKeyPairings.put(end, "End standard");
		pDown = new JButton("Pg Dn");
		thirdRow[16] = pDown;
		buttonKeyPairings.put(pDown, "Page Down standard");
		npSeven = new JButton("7");
		thirdRow[17] = npSeven;
		buttonKeyPairings.put(npSeven, "Home numpad");
		npEight = new JButton("8");
		thirdRow[18] = npEight;
		buttonKeyPairings.put(npEight, "Up numpad");
		npNine = new JButton("9");
		thirdRow[19] = npNine;
		buttonKeyPairings.put(npNine, "Page Up numpad");

		caps = new JButton("Caps Lock");
		fourthRow[0] = caps;
		buttonKeyPairings.put(caps, "Caps Lock standard");
		a = new JButton("A");
		fourthRow[1] = a;
		buttonKeyPairings.put(a, "A standard");
		s = new JButton("S");
		fourthRow[2] = s;
		buttonKeyPairings.put(s, "S standard");
		d = new JButton("D");
		fourthRow[3] = d;
		buttonKeyPairings.put(d, "D standard");
		f = new JButton("F");
		fourthRow[4] = f;
		buttonKeyPairings.put(f, "F standard");
		g = new JButton("G");
		fourthRow[5] = g;
		buttonKeyPairings.put(g, "G standard");
		h = new JButton("H");
		fourthRow[6] = h;
		buttonKeyPairings.put(h, "H standard");
		j = new JButton("J");
		fourthRow[7] = j;
		buttonKeyPairings.put(j, "J standard");
		k = new JButton("K");
		fourthRow[8] = k;
		buttonKeyPairings.put(k, "K standard");
		l = new JButton("L");
		fourthRow[9] = l;
		buttonKeyPairings.put(l, "L standard");
		col = new JButton(";");
		fourthRow[10] = col;
		buttonKeyPairings.put(col, "Semicolon standard");
		apos = new JButton("'");
		fourthRow[11] = apos;
		buttonKeyPairings.put(apos, "Quote standard");
		enter = new JButton("Enter");
		fourthRow[12] = enter;
		buttonKeyPairings.put(enter, "Enter standard");
		npFour = new JButton("4");
		fourthRow[13] = npFour;
		buttonKeyPairings.put(npFour, "Left numpad");
		npFive = new JButton("5");
		fourthRow[14] = npFive;
		buttonKeyPairings.put(npFive, "Clear numpad");
		npSix = new JButton("6");
		fourthRow[15] = npSix;
		buttonKeyPairings.put(npSix, "Right numpad");

		lShift = new JButton("Shift");
		fifthRow[0] = lShift;
		buttonKeyPairings.put(lShift, "Shift left");
		z = new JButton("Z");
		fifthRow[1] = z;
		buttonKeyPairings.put(z, "Z standard");
		x = new JButton("X");
		fifthRow[2] = x;
		buttonKeyPairings.put(x, "X standard");
		c = new JButton("C");
		fifthRow[3] = c;
		buttonKeyPairings.put(c, "C standard");
		v = new JButton("V");
		fifthRow[4] = v;
		buttonKeyPairings.put(v, "V standard");
		b = new JButton("B");
		fifthRow[5] = b;
		buttonKeyPairings.put(b, "B standard");
		n = new JButton("N");
		fifthRow[6] = n;
		buttonKeyPairings.put(n, "N standard");
		m = new JButton("M");
		fifthRow[7] = m;
		buttonKeyPairings.put(m, "M standard");
		comma = new JButton(",");
		fifthRow[8] = comma;
		buttonKeyPairings.put(comma, "Comma standard");
		stop = new JButton(".");
		fifthRow[9] = stop;
		buttonKeyPairings.put(stop, "Period standard");
		slash = new JButton("/");
		fifthRow[10] = slash;
		buttonKeyPairings.put(slash, "Slash standard");
		rShift = new JButton("Shift");
		fifthRow[11] = rShift;
		buttonKeyPairings.put(rShift, "Shift right");
		up = new JButton(Character.toString((char) 8593));
		fifthRow[12] = up;
		buttonKeyPairings.put(up, "Up standard");
		npOne = new JButton("1");
		fifthRow[13] = npOne;
		buttonKeyPairings.put(npOne, "End numpad");
		npTwo = new JButton("2");
		fifthRow[14] = npTwo;
		buttonKeyPairings.put(npTwo, "Down numpad");
		npThree = new JButton("3");
		fifthRow[15] = npThree;
		buttonKeyPairings.put(npThree, "Page Down numpad");

		lCtrl = new JButton("Ctrl");
		sixthRow[0] = lCtrl;
		buttonKeyPairings.put(lCtrl, "Ctrl left");
		lWin = new JButton("Win");
		sixthRow[1] = lWin;
		buttonKeyPairings.put(lWin, "Windows left");
		lAlt = new JButton("Alt");
		sixthRow[2] = lAlt;
		buttonKeyPairings.put(lAlt, "Alt left");
		space = new JButton(" ");
		sixthRow[3] = space;
		buttonKeyPairings.put(space, "Space standard");
		rAlt = new JButton("Alt");
		sixthRow[4] = rAlt;
		buttonKeyPairings.put(rAlt, "Alt right");
		rWin = new JButton("Win");
		sixthRow[5] = rWin;
		buttonKeyPairings.put(rWin, "Windows right");
		rClick = new JButton("Context");
		sixthRow[6] = rClick;
		buttonKeyPairings.put(rClick, "Context Menu standard");
		rCtrl = new JButton("Ctrl");
		sixthRow[7] = rCtrl;
		buttonKeyPairings.put(rCtrl, "Ctrl right");
		left = new JButton(Character.toString((char) 8592));
		sixthRow[8] = left;
		buttonKeyPairings.put(left, "Left standard");
		down = new JButton(Character.toString((char) 8595));
		sixthRow[9] = down;
		buttonKeyPairings.put(down, "Down standard");
		right = new JButton(Character.toString((char) 8594));
		sixthRow[10] = right;
		buttonKeyPairings.put(right, "Right standard");
		npZero = new JButton("0");
		sixthRow[11] = npZero;
		buttonKeyPairings.put(npZero, "Insert numpad");
		npStop = new JButton(".");
		sixthRow[12] = npStop;
		buttonKeyPairings.put(npStop, "Delete numpad");

		npPlus = new JButton("+");
		buttonKeyPairings.put(npPlus, "NumPad + numpad");
		npEnter = new JButton("Enter");
		buttonKeyPairings.put(npEnter, "Enter numpad");
	}
}
