package mapping;
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.util.HashMap;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;

/**
 * This is the presenter in the model-view-presenter architecture. 
 * It acts as the mediator between the model and the view. Its job is 
 * to update the model with information passed from the view, and then 
 * display the state of the model in the view.
 * @author Matthew Henderson - University of Glasgow Masters Project 2018
 */
public class Presenter {
	
	private MainGUI gui;
	private MappingModel model;
	
	/**
	 * Class constructor
	 */
	public Presenter() {
		model = new MappingModel();
		startUp();
	}
	
	/**
	 * The first method called when the object is created. This method 
	 * Displays the window that allows users to import an existing 
	 * XML file or create a completely new map.
	 */
	public void startUp() {
		//Create the window and make it visible
		LoadXmlPopup load = new LoadXmlPopup(this);
		load.setVisible(true);
	}
	
	/**
	 * Called if the user wishes to load a pre-existing XML file. This 
	 * methods informs the parser to parse the selected file and update 
	 * the model with the information.
	 * @param path	the path of the XML file to be parser
	 */
	public void loadExistingFile(String path) {
		//Create a new model object that overwrites previous model
		model = new MappingModel();
		//Populate the model with data from the xml
		XmlParser parser = new XmlParser(path, model);
		parser.parse();
		//Display the key maps
		openMainGUI(null, true);
	}
	
	
	/**
	 * Returns whether the specified key is mapped in the specified keymap.
	 * @param selectedKey	the unique identifier of the key
	 * @param keyMapName	the name of the keymap
	 * @return				returns <code>true</code> if the key is mapped, 
	 * 						<code>false</code> otherwise
	 */
	public boolean isKeyMapped(String selectedKey, String keyMapName) {
		//Get the key and return whether it is mapped
		Key key = model.getKey(selectedKey, keyMapName);
		return key.isMapped();
	}
	
	/**
	 * Opens the window to allow the user to create a new keymap.
	 * @param parent	the parent keymap of the new keymap
	 */
	public void openNewKeyMapWindow(String parent) {
		//Create the window and make it visible
		NewKeyMapPopup newKM = new NewKeyMapPopup(this, parent);
		newKM.setVisible(true);
	}
	
	/**
	 * Creates a new keymap and adds it to the model.
	 * @param name		the name of the new keymap
	 * @param parent	the parent of the keymap
	 * @return			returns <code>true</code> if the keymap has been created, 
	 * 					<code>false</code> otherwise
	 */
	public boolean createNewKeyMap(String name, String parent) {
		//Don't allow keymaps with duplicate names
		if (model.isKeyMapNameAvailable(name)) {
			model.createNewKeyMap(name, parent);
			//Add the keymap to the tree display in the view
			//and update the virtual keyboard
			if (gui != null) {
				gui.addKeymapNode(name);
				loadKeyMap(name);
			}
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Renames a key map if the name is available
	 * @param keymapName	the name of the keymap to be renamed
	 * @param newName		the new name for the keymap
	 * @return				returns <code>true</code> if the keymap has successfully, 
	 * 						renamed <code>false</code> otherwise
	 */
	public boolean renameKeymap(String keymapName, String newName) {
		//First check if the new name is available
		if (model.isKeyMapNameAvailable(newName)) {
			//Get the keymap and set its new name
			KeyMap keymap = model.getKeyMap(keymapName);
			keymap.setName(newName);
			loadKeyMap(newName);
			return true;
		} else {
			//Keymap was not renamed
			return false;
		}
	}
	
	/**
	 * Sets the chosen key as a trigger for a keymap.
	 * @param triggerKey	the key to be set as a trigger
	 * @param keymapName	the name of the keymap
	 */
	public void setTrigger(String triggerKey, String keymapName) {
		//Get the chosen key from the keymap
		KeyMap keymap = model.getKeyMap(keymapName);
		Key key = keymap.getKey(triggerKey);
		key.setAsTrigger();
		
		//Display the key as a trigger key
		setKeyDisplay(key);
	}
	
	/**
	 * Updates the virtual keyboard's display based on the state of the system. 
	 * The method will render mapped keys, trigger keys and randomised keys.
	 * @param key	the key to be displayed
	 */
	private void setKeyDisplay(Key key) {
		//Get the unique identifier of the key
		String keyString = key.getUniqueKeyIdentifier();
		
		//Not all possible keys will be on the keyboard
		if (!gui.hasKey(keyString)) {
			return;
		}
		
		//Only keys that are modified need to be updated from the default display
		if (key.isTrigger() || key.isMapped() || key.isRandomised()) {
			//If the key is set to a custom text display, write the text on the key
			if (key.isTextOverlay()) {
				gui.setKeyDisplayText(keyString, key.getTextOverlay());;
			//If the key has an overlay image assigned to it, render the image on the key
			} else if (key.isImageOverlay()) {
				gui.setKeyImage(keyString, key.getImage());
			//If the key is mapped but does not have a custom display, render the default
			//unicode character
			} else if (key.isMapped()) {
				//Get the unicode string and convert it to an int
				String unicode = key.getMappingUnicode();
				//Convert from unicode string to a character that the button can display
			    String displayText = UnicodeConverter.getJavaUnicodeChar(unicode);
				gui.setKeyDisplayText(keyString, displayText);
			}
		} else {
			gui.removeKeyDisplay(key.getUniqueKeyIdentifier());
		}
		
		//Set the border of the key
		if (key.isTrigger()) {
			gui.setKeyBorder(keyString, new Color(229,0,0));
		} else if (key.isRandomised()) {
			gui.setKeyBorder(keyString, new Color(65,105,225));
		} else if (key.isMapped()) {
			gui.setKeyBorder(keyString, new Color(144,238,144));
		} else {
			gui.setKeyBorder(keyString, null);
		}
	}
	
	/**
	 * Sets the text that will be displayed on a mapped key or a trigger key.
	 * @param selectedKey	the key that will have it's display updated
	 * @param keymap		the keymap that the key belongs to
	 * @param textOverlay	the text to be displayed on the key
	 */
	public void setTextOverlay(String selectedKey, String keymap, String textOverlay) {
		Key key = model.getKey(selectedKey, keymap);
		key.setTextOverlay(textOverlay);
	}
	
	/**
	 * Opens the main interface window containing the virtual keyboard.
	 * @param keymap	the keymap to be displayed when the window first opens
	 * @param newWindow	whether to open the main gui in a new window
	 */
	public void openMainGUI(String keymap, boolean newWindow) {
		//Create the gui and and make it visible if it doesn't exist or
		//if a new window is needed
		if (gui == null || newWindow) {
			gui = new KeyboardDisplay(this, keymap);
			gui.setVisible(true);
		}
	}
	
	/**
	 * Open the window to assign a mapping to a key.
	 * @param selectedKey	the key to be mapped
	 * @param keymapName	the keymap which the key belongs to
	 */
	public void openMappingPopup(String selectedKey, String keymapName) {
		//Find the key object
		Key key = model.getKey(selectedKey, keymapName);
		String displayType;
		//Gets the current mapping of the key or sets the string to none is there isn't one
		String currentMapping = key.isMapped() ? key.getMappingUnicode() : "None";
		//Get the display type of the key
		if (key.isTextOverlay()) {
			displayType = "Text Overlay";
		} else if (key.isImageOverlay()) {
			displayType = "Image";
		} else if (!key.isMapped()) {
			displayType = "None";
		} else {
			displayType = "Default Unicode Representation";
		}
		//Don't allow trigger keys to be mapped
		if (!key.isTrigger()) {
			//Open the advanced mapping popup
			AdvancedMappingPopup picker = new AdvancedMappingPopup(selectedKey, keymapName, displayType, currentMapping, this);
			picker.setVisible(true);
		}
	}
	
	/**
	 * Set's the mapping of a key.
	 * @param selectedKey	the key to be mapped
	 * @param keymap		the keymap which it belongs to
	 * @param mapping		the unicode value for the key to be mapped to
	 */
	public void setKeyMapping(String selectedKey, String keymap, String mapping) {
		//Get the key object, set the mapping and update the virtual keyboard
		Key key = model.getKey(selectedKey, keymap);
		if (!key.isTrigger()) {
			key.setMapping(mapping);
			setKeyDisplay(key);
		}	
	}
	
	/**
	 * Sets an image to be displayed on a key.
	 * @param selectedKey	the key which will have the image displayed on it
	 * @param keymap		the keymap which the key belongs to
	 * @param img			the image to be display on the key
	 */
	public void setKeyImage(String selectedKey, String keymap, BufferedImage img) {
		//Get the key object, set the image and update the virtual keyboard
		Key key = model.getKey(selectedKey, keymap);
		key.setImage(img);
		setKeyDisplay(key);
	}
	
	/**
	 * Build the keymap tree to be displayed in the user interface. 
	 * Not to be confused with the TreeDataStructure, this tree us just a display component.
	 * @return	the JTree to be displayed in the interface.
	 */
	public JTree buildJTree() {
		//Get the data structure from the model
		TreeDataStructure<KeyMap> keymaps = model.getKeyMapTree();
		
		//Create the root node of the tree
		DefaultMutableTreeNode top = new DefaultMutableTreeNode("Keymaps");
		JTree tree = new JTree(top);
		
		
		//Map to store the nodes so that they can be referenced as parent nodes
		HashMap<KeyMap, DefaultMutableTreeNode> nodes = new HashMap<KeyMap, DefaultMutableTreeNode>();
		for (KeyMap km : keymaps) {
			//Create the node of the tree from the keymap name
			DefaultMutableTreeNode keymapNode = new DefaultMutableTreeNode(km.getName());
			//Put the node in the map
			nodes.put(km, keymapNode);
			//Get the parent from the map
			KeyMap parent = keymaps.getParent(km);
			//If the keymap does not have a parent add it to the root node
			if (!nodes.containsKey(parent)) {
				top.add(keymapNode);
			//Add the keymap as a root node of it's parent
			} else {
				DefaultMutableTreeNode parentNode = nodes.get(parent);
				parentNode.add(keymapNode);
			}
		}
		
		return tree;
	}
	
	/**
	 * Displays the chosen keymap on the virtual keyboard.
	 * @param keymapName	the keymap to be displayed on the virtual keyboard
	 */
	public void loadKeyMap(String keymapName) {
		//Reset the display to the default to get rid of previous keymap
		gui.setDefaultDisplay();
		//Get the keymap to be displayed
		KeyMap keymap = model.getKeyMap(keymapName);
		
		//Get the map of keys belonging to the keymap
		HashMap<String, Key> map = keymap.getMap();
		//Sets the current keymap in the gui
		gui.setCurrentKeymap(keymapName);
		
		//Set's the display of all the keys in the keymap
		for (Key key : map.values()) {
		    setKeyDisplay(key);
		}
	}
	
	/**
	 * Remove any custom displays on the specified key.
	 * @param selectedKey	the key to be modified
	 * @param keymap		the keymap which the key belongs to
	 */
	public void removeDisplay(String selectedKey, String keymap) {
		//Get the key and remove it's display
		Key key = model.getKey(selectedKey, keymap);
		key.removeCustomDisplay();
	}
	
	/**
	 * Mark the chosen key as randomised.
	 * @param selectedKey	the key to be randomised
	 * @param keymap		the keymap which the key belongs to
	 */
	public void setAsRandom(String selectedKey, String keymap) {
		//Get the key and randomise it
		Key key = model.getKey(selectedKey, keymap);
		key.randomise();
	}
	
	/**
	 * Delete the specified keymap from the model.
	 * @param keymap	the keymap to be deleted
	 */
	public void deleteKeymap(String keymap) {
		model.deleteKeymap(keymap);
	}
	
	/**
	 * Completely resets a keymap. All mappings, triggers etc are removed.
	 * @param keymap	the name of the keymap
	 */
	public void resetKeymap(String keymap) {
		//Get the keymap and reset it
		KeyMap km = model.getKeyMap(keymap);
		km.reset();
		//Load the keymap to apply the changes
		loadKeyMap(keymap);
	}
	
	/**
	 * Resets a key. removes all display and mapping modifications
	 * @param selectedKey	the identifier of the key
	 * @param keymap		the key map which is belongs to
	 */
	public void resetKey(String selectedKey, String keymap) {
		//Get the key
		Key key = model.getKey(selectedKey, keymap);
		//Remove the modification
		key.restoreToDefault();
		//Apply the changes
		setKeyDisplay(key);
	}
	
	/**
	 * Generate the output XML file.
	 * @param path	the path that the xml file will be output to
	 */
	public void generateXML(String path) {
		//Start the serialise thread running
		XmlSerialiser xml = new XmlSerialiser(model.getKeyMapTree(), path);
		xml.start();
	}
}
