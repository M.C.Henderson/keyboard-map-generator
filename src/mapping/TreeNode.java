package mapping;
import java.util.LinkedList;
import java.util.List;

/**
 * Class for modelling nodes of the TreeDataStructure.
 * @see TreeDataStructure
 * @author Matthew Henderson - University of Glasgow Masters Project 2018
 *
 * @param <T>	the data type to be contained in the node
 */
public class TreeNode<T extends Comparable<T>> {
	private T data;
	private TreeNode<T> parent;
	private LinkedList<TreeNode<T>> children;
	
	/**
	 * Constructs the tree node object.
	 * @param data		the data to be stored in the node
	 * @param parent	the parent node of the new node
	 */
	public TreeNode (T data, TreeNode<T> parent) {
		this.data = data;
		this.parent = parent;
		children = new LinkedList<TreeNode<T>>();
	}
	
	/**
	 * Returns the parent node of the node.
	 * @return	the parent node
	 */
	public TreeNode<T> getParent() {
		return parent;
	}
	
	/**
	 * Removes a child element from the list of children.
	 * @param child	the element to be removed
	 * @return		returns <code>true</code> if the element has, 
	 * 				been successfully removed, <code>false</code> otherwise
	 */
	public boolean removeChild(T child) {
		//Search for the element in the list of children
		for (TreeNode<T> item : children) {
			//Remove the element and return true
			if (item.getData().equals(child)) {
				children.remove(item);
				return true;
			}
		}
		//The element is not a child of the node, return false
		return false;
	}
	
	/**
	 * Creates a node for an element and adds it to the list of children.
	 * @param child	the element to be added as a child
	 */
	public void addChild(T child) {
		//Create the node and add it is a child
		children.add(new TreeNode<T>(child, this));
	}
	
	/**
	 * Returns the list of child nodes.
	 * @return	the list of child nodes
	 */
	public List<TreeNode<T>> getChildren() {
		return children;
	}
	
	/**
	 * Returns whether the node has children.
	 * @return	returns <code>true</code> if the node has children, 
	 * 			<code>false</code> otherwise
	 */
	public boolean hasChildren() {
		return (!children.isEmpty());
	}
	
	/**
	 * Returns the data contained in the node.
	 * @return	the data contained in the node
	 */
	public T getData() {
		return data;
	}
}
