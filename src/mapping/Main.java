package mapping;

/**
 * The entry point to the program. Creates the model and presenter classes.
 * @author Matthew Henderson - University of Glasgow Masters Project 2018
 *
 */
public class Main {
	
	/**
	 * Main method
	 * @param args	Command line arguments.
	 */
	
	
	public static void main(String[] args) {
		//Create the presenter. this will in turn create the model and view
		Presenter presenter = new Presenter();
	}
}
