package mapping;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import javax.imageio.ImageIO;
import javax.swing.JOptionPane;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.DocumentType;
import org.w3c.dom.Element;

/**
 * Class to serialise the user defined keymaps into XML. It creates an XML file in the 
 * output folder. Key overlay images are also placed in the output folder.  The XML follows 
 * the document type definition file schema which can be found in the resources folder or 
 * in the project report. This class is threaded.
 * @author Matthew Henderson - University of Glasgow Masters Project 2018
 */
public class XmlSerialiser extends Thread {
	
	private TreeDataStructure<KeyMap> keymaps;
	private Document doc;
	private String path;
	
	/**
	 * Constructs the XMLSerialiser object.
	 * @param keymaps	the tree of keymaps to be serialised
	 * @param path		the path that the configuration files will be saved in
	 */
	public XmlSerialiser(TreeDataStructure<KeyMap> keymaps, String path) {
		this.keymaps = keymaps;
		this.path = path;
	}
	
	/**
	 * Creates the mapping XML file in the output folder of the project. This method is 
	 * threaded so as not to freeze the user interface, especially when multiple image 
	 * files need to be created.
	 */
	public void run() {
		try {
			//Use the document factory builder to create a document object which will be used
			//To specify the XML
		    DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		    DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
		    doc = docBuilder.newDocument();

		    //Call helper method to add elements to the document
		    populateElements();

		    //Prepare to write the content into xml file
		    TransformerFactory transformerFactory =  TransformerFactory.newInstance();
		    Transformer transformer = transformerFactory.newTransformer();
		    transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		    transformer.setOutputProperty(OutputKeys.METHOD, "xml");
		    
		    //Reference the dtd in the 
		    DOMImplementation domImpl = doc.getImplementation();
		    DocumentType doctype = domImpl.createDocumentType("doctype",
		        "",
		        System.getProperty("user.dir") + "\\res\\dtd\\mappings.dtd");
		    transformer.setOutputProperty(OutputKeys.DOCTYPE_SYSTEM, doctype.getSystemId());
		    
		    DOMSource source = new DOMSource(doc);
		    
		    File filePath = new File(path);
		    //Make the directories if they don't already exist
			if (!filePath.exists()) {
				filePath.mkdirs();
			} 

		    //Create the XML file in the output folder
		    StreamResult result =  new StreamResult(new File(path + "\\mappings.xml"));
		    transformer.transform(source, result);
		    
		//Display error messages if the data cannot be serialised
		}catch(ParserConfigurationException e){
			JOptionPane.showMessageDialog(null, "There was a problem creating the XML", "warning", JOptionPane.PLAIN_MESSAGE);
		}catch(TransformerException e){
			JOptionPane.showMessageDialog(null, "There was a problem creating the XML", "warning", JOptionPane.PLAIN_MESSAGE);
		}
	}
	
	/**
	 * For all the keymaps in the tree, create an element in the document.
	 */
	private void populateElements() {
		//Set up the root element
		Element rootElement = doc.createElement("mappings");
		doc.appendChild(rootElement);
		
		//Store elements in a hash map so that parent elements an be quickly found
		HashMap<KeyMap, Element> elements = new HashMap<KeyMap, Element>();
		
		//Loop through all the keymaps in the tree structure
		for (KeyMap keymap : keymaps) {
			Element element;
			//If the keymap's parent is null it is a top level element
			if (keymaps.getParent(keymap) == null) {
				//Use helper method to add element to the document
				element = generateKeymapElement(keymap, rootElement);
			} else {
				//Element has a parent. Find in the HashMap
				KeyMap parent = keymaps.getParent(keymap);
				Element parentElement = elements.get(parent);
				//Use helper method to add element to the document
				element = generateKeymapElement(keymap, parentElement);
			}
			//Put the element in the map in case it is needed as a parent for another element
			elements.put(keymap, element);
		}
	}
	
	/**
	 * Add keymap and child elements to the doucment
	 * @param keymap	the keymap element to be added
	 * @param parent	the parent element of the keymap element
	 * @return
	 */
	private Element generateKeymapElement(KeyMap keymap, Element parent) {
		//Create a keymap element in the document and set it's name attribute
		Element keymapElement = doc.createElement("keymap");
	    parent.appendChild(keymapElement);
	    keymapElement.setAttribute("name", keymap.getName());
		
	    //Get the hashmap of keys
	    HashMap<String, Key> keyHashMap = keymap.getMap();
	    
	    //Loop through all the keys in the document
	    for (Key key : keyHashMap.values()) {
	    	Element keyElement;
	    	
	    	//Only add modified keys to the document
	    	//Add trigger keys as trigger elements
	    	if (key.isTrigger()) {
	    		keyElement = addTriggerElement(key);
	    	//Add mapped and randomised keys as key elements
	    	} else if (key.isMapped() || key.isRandomised()) {
	    		keyElement = addKeyElement(key);
	    	} else {
	    		//Key not modified, skip to next key
	    		continue;
	    	}
	    	
	    	//Add attributes common to both trigger and key elements
	    	addKeyIdentifierAttributes(key, keyElement);
	    	addKeyDisplayContent(key, keyElement, keymap.getName());
	    	keymapElement.appendChild(keyElement);
	    }
	    return keymapElement;
	}
	
	/**
	 * Helper method to place key overlay images in the output folder.
	 * @param key			the key which has the image overlay
	 * @param keymapName	the name of the keymap which the key belongs to
	 * @return				the path of the image file created
	 */
	private String outputImageToFolder(Key key, String keymapName) {
		//Create a unique filename from the combination of keymap name, key name and key location
		String imagePath = "\\images\\" + keymapName + "\\" + key.getKeyName() + "_" + key.getLocation() + ".jpg";
		//Create a new file for the image within the keyboard mapping folder
		File outputfile = new File(path + imagePath);
		
		//Make the directories if they don't already exist
		if (!outputfile.exists()) {
		    outputfile.mkdirs();
		} 
		try {
			//Create the image file
			ImageIO.write(key.getImage(), "jpg", outputfile);
		//Display error message of the file cannot be written
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, "There was creating the image files",
					"warning", JOptionPane.PLAIN_MESSAGE);
		}
		return imagePath;
	}
	
	/**
	 * Creates a trigger XML element. Doesn't do much now but may be useful for further
	 * development when triggers are more complicated.
	 * @param key	the key object that contains the data to be serialised
	 * @return		the newly created XML element
	 */
	private Element addTriggerElement(Key key) {
		//Create trigger element
		Element keyElement = doc.createElement("trigger");
		return keyElement;
	}
	
	/**
	 * Creates a key XML element.
	 * @param key	the key object that contains the data to be serialised
	 * @return		the newly created XML element
	 */
	private Element addKeyElement(Key key) {
		Element keyElement = doc.createElement("key");
		//Only add the mapping unicode if one exists.
		if (key.getMappingUnicode() != null && !key.getMappingUnicode().equals("")) {
			//Serialise the mapping unicode
			Element mapping = doc.createElement("mapping");
	        mapping.appendChild(doc.createTextNode(key.getMappingUnicode()));
	        keyElement.appendChild(mapping);
		}
		
		if (key.isRandomised()) {
    		//Set key to randomised
    		keyElement.setAttribute("randomised", "true");
    	} else {
    		//Not randomised
    		keyElement.setAttribute("randomised", "false");
    	}
		
		return keyElement;
	}
	
	/**
	 * Adds the attributes that identifies the keys position on the keyboard.
	 * @param key			the key object that contains the data to be serialised
	 * @param keyElement	the XML element to add the attributes to
	 */
	private void addKeyIdentifierAttributes(Key key, Element keyElement) {
		//Add attributes
    	keyElement.setAttribute("name", key.getKeyName());
    	keyElement.setAttribute("location", key.getLocation());
    	keyElement.setAttribute("VK_keyCode", "" + key.getVkKeyCode());
	}
	
	/**
	 * Serialises the display content of the key.
	 * @param key			the key object that contains the data to be serialised
	 * @param keyElement	the XML element that the display data will be added to
	 * @param keymapName	the name of the keymap that the keys belongs to
	 */
	private void addKeyDisplayContent(Key key, Element keyElement, String keymapName) {
		//Set the display content for the key
        if (key.getDisplayType() == Key.DisplayType.TEXT) {
        	//The key has a text overlay. Create the element and add to the document
        	Element text = doc.createElement("text");
        	text.appendChild(doc.createTextNode(key.getTextOverlay()));
        	keyElement.appendChild(text);
        } else if (key.getDisplayType().equals(Key.DisplayType.IMAGE)) {
        	//The key has a image overlay. Create the img element and add to the document
        	//Also place the image in the output folder
        	String imagePath = outputImageToFolder(key, keymapName);
        	Element img = doc.createElement("img");
        	img.setAttribute("src", imagePath);
        	keyElement.appendChild(img);
        }
	}
}
