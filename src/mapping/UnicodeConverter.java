package mapping;

/**
 * Utility class to convert to and from unicode hex representation. Also 
 * allows checking of valid Unicode. Note that the notation /u0000 is preferred 
 * here instead of the more conventional U+0000. This is simply to be consistent 
 * with the notation that Java uses.
 * @author Matthew Henderson - University of Glasgow Masters Project 2018
 */
public class UnicodeConverter {
	
	/**
	 * Returns the unicode representation of a given character.
	 * @param character	the integer representation of the character
	 * @return			the unicode String
	 */
	public static String convertIntToHex(int character) {
		String hex;
		//For characters that can be stored in than 16 bits, pad with 0's
		if (character <= 65535) {
			hex = "\\u" + Integer.toHexString(character | 0x10000).substring(1);
		//Characters that require more that 16 bits. This approach does not truncate hex string
		} else {
			hex = "\\u" + Integer.toHexString(character);
		}
		return hex;
	}
	
	/**
	 * Returns the integer representation of a unicode character. 
	 * Returns zero if the String is not a valid unicode string.
	 * @param unicode	the unicode string
	 * @return			the integer representation of the character
	 */
	public static int convertHexToInt(String unicode) {
		int charInt = -1;
		//Perform loose check that the hex string is in a unicode format. Return 0 if not
		if (unicode != null && unicode.length() > 1 && unicode.substring(0, 2).equals("\\u")) {
			//Remove unicode prefix and parse hex string to an integer
			unicode = unicode.substring(2, unicode.length());
			try {
				charInt =  Integer.parseInt(unicode, 16);
			//Return 0 if the hex String is in the wrong format
			} catch (NumberFormatException e) {
				return charInt;
			}
			
		}
		return charInt;
	}
	
	/**
	 * Checks if a string is a valid Unicode hexadecimal. 
	 * Note that this is the string in the format \\u + hex.
	 * @param hex	the string to be checked
	 * @return		returns <code>true</code> if the string is a a valid 
	 * 				Hexadecimal string, <code>false</code> otherwise
	 */
	public static boolean isUnicodeHexString(String hex) {
		//Return false for null string
		if (hex == null) {
			return false;
		}
		//Check the string against the hex regex
		boolean isHex = hex.matches("\\\\u\\p{XDigit}+");
		if (isHex) {
			//The maximum value of a unicode character is 1114111
			//Return false if the hex string is larger 
			int value = convertHexToInt(hex);
			isHex = (inUnicodeIntegerRange(value)) ? true : false;
		}
		return isHex;
	}
	
	/**
	 * Returns the character representation of a Unicode integer as a string. 
	 * e.g. 65 would return "A". This is useful because java components don't 
	 * display characters so a string must be made.
	 * @param unicodeInt	the Unicode integer
	 * @return				the character representation of the Unicode
	 */
	public static String getJavaUnicodeChar(int unicodeInt) {
		//Check the integer is within the valid range
		if (inUnicodeIntegerRange(unicodeInt)) {
			//Create a string buffer to build the string
			StringBuffer sb = new StringBuffer();
			//append the character to the string
		    sb.append(Character.toChars(unicodeInt));
		    String unicodeChar = sb.toString();
		    return unicodeChar;
		}
		return "";
	}
	
	/**
	 * Returns the character representation of a Unicode hex as a string. 
	 * eg \u0041 would return "A". This is useful because java components don't 
	 * display characters so a string must be made.
	 * @param unicodeString	the unicode hex string
	 * @return				the character representation of the hex
	 */
	public static String getJavaUnicodeChar(String unicodeString) {
		//Convert to an int and get the string
		int unicodeInt = convertHexToInt(unicodeString);
		String unicodeChar = getJavaUnicodeChar(unicodeInt);
		return unicodeChar;
	}
	
	/**
	 * Checks whether the integer is within the range of accepted Unicode values
	 * @param value	the integer value to be checked
	 * @return		returns <code>true</code> if the integer is within the 
	 * 				range of Unicode values, <code>false</code> otherwise
	 */
	private static boolean inUnicodeIntegerRange(int value) {
		//All Unicode values are between 0 and 1114111
		if (value < 0 || value > 1114111) {
			return false;
		} else {
			return true;
		}
	}
}
