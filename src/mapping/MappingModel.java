package mapping;
/**
 * Class to represent the hierarchy of keyboard maps.
 * @author Matthew Henderson - University of Glasgow Masters Project 2018
 */
public class MappingModel {
	
	private TreeDataStructure<KeyMap> keymaps;
	
	/**
	 * Class constructor.
	 */
	public MappingModel() {
		keymaps = new TreeDataStructure<KeyMap>();
	}
	
	/**
	 * Returns whether the keymap name is available.
	 * @param keyMapName	the proposed name of the keymap
	 * @return				returns <code>true</code> if the keymap name is   
	 * 						available, <code>false</code> otherwise
	 */
	public boolean isKeyMapNameAvailable(String keyMapName) {
		boolean available = true;
		//Check all the maps in the structure
		for (KeyMap keymap : keymaps) {
		    if (keymap.getName().equals(keyMapName)) {
		    	//The name is already used, return false
		    	available = false;
		    }
		}
		return available;
	}
	
	/**
	 * Creates a new keymap and adds it the keymap tree.
	 * @param name		the name of the keymap to be created
	 * @param parent	the name of the keymap that will be the parent
	 * 					of the new keymap. To add the keymap at the top
	 * 					of the tree pass <code>null</code>
	 * @return			the newly created KeyMap object
	 */
	public KeyMap createNewKeyMap(String name, String parent) {
		//First check that the name is available
		if (!isKeyMapNameAvailable(name)) {
			//Return null if the name is in use
			return null;
		}
		//Create the object
		KeyMap keymap = new KeyMap(name);
		//If no parent is specified add the keymap to the top level of the tree
		if (parent == null) {
			keymaps.add(keymap, null);
		//Add the keymap to the tree as a child of the parent
		} else {
			//Get the parent and add as a child
			KeyMap parentKM = getKeyMap(parent);
			keymaps.add(keymap, parentKM);
		}
		return keymap;
	}
	
	/**
	 * Returns the Key object for a specified key and specified keymap. 
	 * @param selectedKey	the String that uniquely identifies the key
	 * @param keyMapName	the name of the keymap that the key is associated with
	 * @return				the Key object
	 */
	public Key getKey(String selectedKey, String keyMapName) {
		//Get the keymap and then search for the key
		KeyMap keymap = getKeyMap(keyMapName);
		if (keymap != null) {
			//return the key from the keymap
			return keymap.getKey(selectedKey);
		}
		//Return null if there is no match
		return null;
	}
	
	/**
	 * Returns the KeyMap object of the specified name.
	 * @param keyMapName	the name of the keymap
	 * @return				the KeyMap object associated with the specified name
	 */
	public KeyMap getKeyMap(String keyMapName) {
		for (KeyMap keymap : keymaps) {
			//Loop through all keymaps and return the one with the matching name
		    if (keymap.getName().equals(keyMapName)) {
		    	return keymap;
		    }
		}
		//Return null if there is no keymap with the specified name
		return null;
	}
	
	/**
	 * Returns whether the keymap tree is empty.
	 * @return	returns <code>true</code> if the keymap tree is empty,   
	 * 			<code>false</code> otherwise
	 */
	public boolean isEmpty() {
		return keymaps.isEmpty();
	}
	
	/**
	 * Returns the tree of keymaps.
	 * @return	all keymaps in the model stored in a custom tree representation
	 */
	public TreeDataStructure<KeyMap> getKeyMapTree() {
		return keymaps;
	}
	
	/**
	 * Deletes a keymap from the model.
	 * @param keymap	the name of the keymap to be deleted
	 */
	public void deleteKeymap(String keymap) {
		//Remove the keymap and remove it from the tree
		KeyMap delete = getKeyMap(keymap);
		keymaps.remove(delete);
	}
}
