package mapping;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import javax.imageio.ImageIO;
import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.ToolTipManager;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 * Pop up window that allows users to create a new keymap. The allows users to 
 * name the map (name cannot already exist) and choose the trigger keys for the 
 * map. These are the keys that will activate the map. A display can also be chosen 
 * for the trigger keys. This can either be image, text or none and defines what 
 * will be rendered on the trigger key.
 * @author Matthew Henderson - University of Glasgow Masters Project 2018
 *
 */
public class NewKeyMapPopup extends JFrame implements ActionListener, KeyListener {

	//Layout components
	private JLabel nameLabel, radioLabel;
	private JRadioButton noTrigger, oneTrigger, twoTrigger, threeTrigger;
	private JTextField name, triggerKeyOne, textOverlayOne, textOverlayTwo, triggerKeyTwo, triggerKeyThree, textOverlayThree;
	private JButton save, cancel;
	private Presenter presenter;
	private JPanel window, centerPanel, form, buttonContainer; 
	private ButtonGroup group;
	private GridBagConstraints layoutConstraints;
	private JComboBox<String> triggerComboOne, triggerComboTwo, triggerComboThree;
	private JPanel keyPanelOne, keyPanelTwo, keyPanelThree, imagePanelOne, imagePanelTwo, imagePanelThree;
	private JPanel textPanelOne, textPanelTwo, textPanelThree, comboPanelOne, comboPanelTwo, comboPanelThree;
	
	//Map of images selected for each trigger key
	private HashMap<String, BufferedImage> images;
	
	//The unique identifiers of each of the trigger keys
	private String keyStringOne, keyStringTwo, keyStringThree, parent;

	/**
	 * Constructs the object
	 * @param presenter	the presenter class in the mvp architecture
	 * @param parent	the parent of the keymap that will be created
	 */
	public NewKeyMapPopup(Presenter presenter, String parent) {
		this.presenter = presenter;
		this.parent = parent;
		//Hashmap to store images
		images = new HashMap<String, BufferedImage>();

		//Set up the JFrame
		setTitle("Create Keymap");
		setLayoutComponents();

		//Resizes window to the components
		pack();
	}

	/**
	 * Populates the JFrame with layout components
	 */
	private void setLayoutComponents() {
		//Create the main JPanel the window
		window = new JPanel(new BorderLayout());
		//Create panel to display the central content
		centerPanel = new JPanel();
		//Create the panel to contain the form components
		form = new JPanel(new GridBagLayout());
		//Set a 10 pixel margin below each form component
		layoutConstraints = new GridBagConstraints();
		layoutConstraints.insets = new Insets(0, 0, 10, 0);
		//Add the form group to the centre of the window 
		centerPanel.add(form);
		window.add(centerPanel, BorderLayout.CENTER);
		this.add(window);

		//Populate the window with components
		addKeymapNamingComponents();
		addTriggerRadioGroup();
		addTriggerOneSelector();
		addTriggerTwoSelector();
		addTriggerThreeSelector();
		addButtons();
	}

	/**
	 * Adds the components that allow the user to set the name 
	 * of the keymap.
	 */
	private void addKeymapNamingComponents() {
		//Add text input and a label
		nameLabel = new JLabel("Name of keymap: ");
		name = new JTextField(20);

		//Set up a panel with the label and text box
		JPanel namePanel = createLabelComponentPanel(nameLabel, name);

		//Put the naming components at the top of the form
		layoutConstraints.anchor = GridBagConstraints.LINE_START;
		layoutConstraints.gridx = 0;
		layoutConstraints.gridy = 0;
		form.add(namePanel, layoutConstraints);
	}

	/**
	 * Add the group of radio buttons that allows a user to select 
	 * the number of trigger keys
	 */
	private void addTriggerRadioGroup() {
		//Create and label the radio buttons
		buttonContainer = new JPanel();
		radioLabel = new JLabel("Number of Trigger Keys: ");
		noTrigger = new JRadioButton("None");
		oneTrigger = new JRadioButton("1");
		twoTrigger = new JRadioButton("2");
		threeTrigger = new JRadioButton("3");
		//Make one trigger button the default
		noTrigger.setSelected(true);
		
		//Add an info hover element to help user with terminology
		JLabel info = new JLabel("<HTML><U>Info</U></HTML>");
		info.setForeground(Color.BLUE);
		String text = "<html>The trigger key is the key that must be pressed to activate the key map."
				+ "<br>If no trigger key is chosen the key map will always be active."
				+ "<br>If multiple keys are chosen, they must all be pressed for the key map to become active."
				+ "<br>The display option refers to what will be rendered upon the trigger key itself.</html>";
		//Add the info as a tool tip
		info.setToolTipText(text);
		
		//Change the timings of the tool tip
		//Make it become instantly visible and stay indefinitely
		ToolTipManager.sharedInstance().setDismissDelay(Integer.MAX_VALUE);
		ToolTipManager.sharedInstance().setInitialDelay(0);

		//Add all the button to a group
		group = new ButtonGroup();
		group.add(noTrigger);
		group.add(oneTrigger);
		group.add(twoTrigger);
		group.add(threeTrigger);

		//Put the buttons in a container 
		buttonContainer.add(noTrigger);
		buttonContainer.add(oneTrigger);
		buttonContainer.add(twoTrigger);
		buttonContainer.add(threeTrigger);
		buttonContainer.add(info);

		//Put the radio buttons in a panel with the label
		JPanel radioPanel = createLabelComponentPanel(radioLabel, buttonContainer);

		//Add the container to the form group
		layoutConstraints.anchor = GridBagConstraints.LINE_START;
		layoutConstraints.gridx = 0;
		layoutConstraints.gridy = 1;
		form.add(radioPanel, layoutConstraints);

		//Add the listeners to the buttons
		addRadioActionListeners();
	}

	/**
	 * Adds action listener to the radio button components
	 */
	private void addRadioActionListeners() {
		//No trigger key option is selected
		noTrigger.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e)
			{
				//Hide all the trigger key selectors
				toggleTriggerOne(false);
				toggleTriggerTwo(false);
				toggleTriggerThree(false);
				pack();
			}
		});
		//One trigger key option is selected
		oneTrigger.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e)
			{
				//Only show selector for one trigger key
				toggleTriggerOne(true);
				toggleTriggerTwo(false);
				toggleTriggerThree(false);
				pack();
			}
		});
		//Two trigger keys option is selected
		twoTrigger.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e)
			{
				//Show the selectors for two trigger keys
				toggleTriggerOne(true);
				toggleTriggerTwo(true);
				toggleTriggerThree(false);
				pack();
			}
		});
		//The three trigger keys option is selected
		threeTrigger.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e)
			{
				//Show the selectors for all three trigger keys
				toggleTriggerOne(true);
				toggleTriggerTwo(true);
				toggleTriggerThree(true);
				pack();
			}
		});
	}
	
	/**
	 * Toggle the visibility of trigger key selection components.
	 * @param visibility	whether the components should be made visible
	 * @param combo			the display selection combo box
	 * @param comboPanel	the panel containing the combo box
	 * @param keyPanel		the panel containing the key selection components
	 * @param textPanel		the panel containing the components for selecting the text overlay
	 * @param imagePanel	the panel containing the components for selecting the image overlay
	 */
	private void toggleTrigger(boolean visibility, JComboBox combo, JPanel comboPanel, JPanel keyPanel, JPanel textPanel, JPanel imagePanel) {
		if (visibility == false) {
			//Hide all the components
			comboPanel.setVisible(visibility);;
			keyPanel.setVisible(visibility);
			textPanel.setVisible(visibility);
			imagePanel.setVisible(visibility);
		} else {
			//Show the display type and trigger key selector
			comboPanel.setVisible(visibility);
			keyPanel.setVisible(visibility);
			//Toggle the other inputs dependent upon the display option the
			//user has selected
			if (combo.getSelectedItem().equals("None")) {
				//Keep text and image input hidden
				textPanel.setVisible(!visibility);
				imagePanel.setVisible(!visibility);
			} else if (combo.getSelectedItem().equals("Image Overlay")) {
				//Keep text input hidden
				textPanel.setVisible(!visibility);
				imagePanel.setVisible(visibility);
			} else {
				textPanel.setVisible(visibility);
				//keep image input hidden
				imagePanel.setVisible(!visibility);
			}
		}
	}

	/**
	 * Toggle the visibility of the inputs for the first trigger key.
	 * @param visibility	whether the inputs for the first trigger
	 * 						key should be visible
	 */
	private void toggleTriggerOne(boolean visibility) {
		toggleTrigger(visibility, triggerComboOne, comboPanelOne, keyPanelOne, textPanelOne, imagePanelOne);
	}

	/**
	 * Toggle the visibility of the inputs for the second trigger key.
	 * @param visibility	whether the inputs for the second trigger
	 * 						key should be visible
	 */
	private void toggleTriggerTwo(boolean visibility) {
		toggleTrigger(visibility, triggerComboTwo, comboPanelTwo, keyPanelTwo, textPanelTwo, imagePanelTwo);
	}

	/**
	 * Toggle the visibility of the inputs for the third trigger key.
	 * @param visibility	whether the inputs for the third trigger
	 * 						key should be visible
	 */
	private void toggleTriggerThree(boolean visibility) {
		toggleTrigger(visibility, triggerComboThree, comboPanelThree, keyPanelThree, textPanelThree, imagePanelThree);
	}

	/**
	 * Adds the inputs for the selection first trigger key
	 */
	private void addTriggerOneSelector() {
		//Create the necessary input components
		triggerKeyOne = new JTextField(20);
		triggerComboOne = new JComboBox<String>();
		textOverlayOne = new JTextField(20);

		//Create panels with labels for the inputs. Having the components 
		//in a panel makes them easier to toggle
		comboPanelOne = getDisplaySelectorPanel(triggerComboOne);
		keyPanelOne = getKeySelectorPanel(triggerKeyOne);
		textPanelOne = getTextSelectorPanel(textOverlayOne);
		imagePanelOne = getImageSelectorPanel("One");
		//Initially hide the text and image inputs
		textPanelOne.setVisible(false);
		imagePanelOne.setVisible(false);

		//Toggle the inputs dependent upon the display type chosen
		triggerComboOne.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e)
			{
				@SuppressWarnings("unchecked")
				//Get the user selection
				JComboBox<String> cb = (JComboBox<String>)e.getSource();
				String overlayStyle = (String)cb.getSelectedItem();

				//If the user has chosen default display, hide all options
				//for display selection
				if (overlayStyle.equals("None")) {
					textPanelOne.setVisible(false);
					imagePanelOne.setVisible(false);
					//Image overlay has been chosen, activate components
					//for choosing images
				} else if (overlayStyle.equals("Image Overlay")) {
					textPanelOne.setVisible(false);
					imagePanelOne.setVisible(true);
					//Test overlay has been chosen, activate components
					//for defining text
				} else {
					textPanelOne.setVisible(true);
					imagePanelOne.setVisible(false);
				}
				pack();
			}
		});

		//Add the components to the form
		layoutConstraints.gridx = 0;
		layoutConstraints.gridy = 2;
		form.add(comboPanelOne, layoutConstraints);
		layoutConstraints.gridy = 3;
		form.add(keyPanelOne, layoutConstraints);
		layoutConstraints.gridy = 4;
		form.add(textPanelOne, layoutConstraints);
		layoutConstraints.gridy = 5;
		form.add(imagePanelOne, layoutConstraints);

		//Initially hide all the inputs
		toggleTriggerOne(false);
	}

	/**
	 * Adds the inputs for the selection second trigger key
	 */
	private void addTriggerTwoSelector() {
		//Create the necessary input components
		triggerKeyTwo = new JTextField(20);
		triggerComboTwo = new JComboBox<String>();
		textOverlayTwo = new JTextField(20);

		//Create panels with labels for the inputs. Having the components 
		//in a panel makes them easier to toggle
		comboPanelTwo = getDisplaySelectorPanel(triggerComboTwo);
		keyPanelTwo = getKeySelectorPanel(triggerKeyTwo);
		textPanelTwo = getTextSelectorPanel(textOverlayTwo);
		imagePanelTwo = getImageSelectorPanel("Two");
		//Initially hide the text and image inputs
		textPanelTwo.setVisible(false);
		imagePanelTwo.setVisible(false);

		//Toggle the inputs dependent upon the display type chosen
		triggerComboTwo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e)
			{
				@SuppressWarnings("unchecked")
				//Get the user selection
				JComboBox<String> cb = (JComboBox<String>)e.getSource();
				String overlayStyle = (String)cb.getSelectedItem();

				//If the user has chosen default display, hide all options
				//for display selection
				if (overlayStyle.equals("None")) {
					textPanelTwo.setVisible(false);
					imagePanelTwo.setVisible(false);
					//Image overlay has been chosen, activate components
					//for choosing images
				} else if (overlayStyle.equals("Image Overlay")) {
					textPanelTwo.setVisible(false);
					imagePanelTwo.setVisible(true);
					//Test overlay has been chosen, activate components
					//for defining text
				} else {
					textPanelTwo.setVisible(true);
					imagePanelTwo.setVisible(false);
				}
				pack();
			}
		});

		//Add the components to the form
		layoutConstraints.gridx = 0;
		layoutConstraints.gridy = 6;
		form.add(comboPanelTwo, layoutConstraints);
		layoutConstraints.gridy = 7;
		form.add(keyPanelTwo, layoutConstraints);
		layoutConstraints.gridy = 8;
		form.add(textPanelTwo, layoutConstraints);
		layoutConstraints.gridy = 9;
		form.add(imagePanelTwo, layoutConstraints);

		//Initially hide all the inputs
		toggleTriggerTwo(false);
	}

	/**
	 * Adds the inputs for the selection third trigger key
	 */
	private void addTriggerThreeSelector() {
		//Create the necessary input components
		triggerKeyThree = new JTextField(20);
		triggerComboThree = new JComboBox<String>();
		textOverlayThree = new JTextField(20);

		//Create panels with labels for the inputs. Having the components 
		//in a panel makes them easier to toggle
		comboPanelThree = getDisplaySelectorPanel(triggerComboThree);
		keyPanelThree = getKeySelectorPanel(triggerKeyThree);
		textPanelThree = getTextSelectorPanel(textOverlayThree);
		imagePanelThree = getImageSelectorPanel("Three");
		//Initially hide the text and image inputs
		textPanelThree.setVisible(false);
		imagePanelThree.setVisible(false);

		//Toggle the inputs dependent upon the display type chosen
		triggerComboThree.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e)
			{
				@SuppressWarnings("unchecked")
				//Get the user selection
				JComboBox<String> cb = (JComboBox<String>)e.getSource();
				String overlayStyle = (String)cb.getSelectedItem();

				//If the user has chosen default display, hide all options
				//for display selection
				if (overlayStyle.equals("None")) {
					textPanelThree.setVisible(false);
					imagePanelThree.setVisible(false);
					//Image overlay has been chosen, activate components
					//for choosing images
				} else if (overlayStyle.equals("Image Overlay")) {
					textPanelThree.setVisible(false);
					imagePanelThree.setVisible(true);
					//Test overlay has been chosen, activate components
					//for defining text
				} else {
					textPanelThree.setVisible(true);
					imagePanelThree.setVisible(false);
				}
				pack();
			}
		});

		//Add the components to the form
		layoutConstraints.gridx = 0;
		layoutConstraints.gridy = 10;
		form.add(comboPanelThree, layoutConstraints);
		layoutConstraints.gridy = 11;
		form.add(keyPanelThree, layoutConstraints);
		layoutConstraints.gridy = 12;
		form.add(textPanelThree, layoutConstraints);
		layoutConstraints.gridy = 13;
		form.add(imagePanelThree, layoutConstraints);

		//Initially hide all the inputs
		toggleTriggerThree(false);
	}

	/**
	 * Populates a display type combo box with elements and add it 
	 * to a panel with a label. Having the components in a panel makes 
	 * them easier to toggle.
	 * @param combo	the combo box to be populated
	 * @return		the panel with the populated combo box and label
	 */
	private JPanel getDisplaySelectorPanel(JComboBox<String> combo) {
		//The label for the combo box
		JLabel comboLabel = new JLabel("Select Display Style: ");
		//Combo box options
		String[] options = {"None", "Text Overlay", "Image Overlay"};

		//Add elements to the combo box and set its size
		combo.setModel(new DefaultComboBoxModel<String>(options));
		combo.setPreferredSize(new Dimension(220, 20));

		//Return the panel complete with label
		return createLabelComponentPanel(comboLabel, combo);
	}

	/**
	 * Creates a panel combining a label and a component.
	 * @param label		the label to be added to the panel
	 * @param component	the component to be added to the panel
	 * @return			the panel that combines the label and component
	 */
	private JPanel createLabelComponentPanel(JLabel label, JComponent component) {
		//Create the panel
		JPanel panel = new JPanel(new GridBagLayout());
		//Set the size of the label so that all columns line up
		label.setPreferredSize(new Dimension(160, 20));

		//Add the label and component to the panel
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.anchor = GridBagConstraints.LINE_END;
		constraints.gridx = 0;
		constraints.gridy = 0;
		panel.add(label, constraints);
		constraints.gridx = 1;
		constraints.anchor = GridBagConstraints.LINE_START;
		panel.add(component, constraints);

		return panel;
	}

	/**
	 * Create a panel containing a text box that listens for key input. 
	 * This text box will be used to allow the user to select the trigger 
	 * key. A label is also added to the panel. Having the components 
	 * in a panel makes them easier to toggle.
	 * @param triggerInput	the text box that will be used to select the
	 * 						trigger key
	 * @return				the panel containing the text box and a label
	 */
	private JPanel getKeySelectorPanel(JTextField triggerInput) {
		//Create the label
		JLabel triggerLabel = new JLabel("Trigger Key (Type Key): ");
		//Set the text box to listen for key presses
		triggerInput.addKeyListener(this);

		return createLabelComponentPanel(triggerLabel, triggerInput);
	}

	/**
	 * Creates a panel that contains a labelled text box for user input. 
	 * Having the components in a panel makes them easier to toggle.
	 * @param textInput	the text box
	 * @return			the panel containing the text box and the label
	 */
	private JPanel getTextSelectorPanel(JTextField textInput) {
		//Create the label
		JLabel textLabel = new JLabel("Text Overlay: ");

		return createLabelComponentPanel(textLabel, textInput);
	}

	/**
	 * Create a panel containing an image selector interface 
	 * complete with labels and listener. Having the components 
	 * in a panel makes them easier to toggle.
	 * @param triggerKeyIdentifier	the string to identify the trigger key the image
	 * 								will be associated e.g. One, Two, Three
	 * @return						the panel containing the image selector and label
	 */
	private JPanel getImageSelectorPanel(String triggerKeyIdentifier) {
		//Filter the file chooser to only show image files
		FileFilter imageFilter = new FileNameExtensionFilter("Image files", ImageIO.getReaderFileSuffixes());
		//Create the file chooser
		JFileChooser imageChooser = new JFileChooser();
		//Attach filter to JFileChooser object
		imageChooser.setFileFilter(imageFilter);
		//Add file chooser button and labels
		JLabel chooseImage = new JLabel("Choose Image: ");
		JButton chooseImageButton = new JButton("Browse...");
		//Label to display the selected image
		JLabel displayImagePath = new JLabel();
		//Container for image components
		JPanel chooseImagePanel = new JPanel();
		chooseImagePanel.add(chooseImageButton); 
		chooseImagePanel.add(displayImagePath);

		//Add action listener to choose image button
		chooseImageButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e)
			{
				//Open the file chooser
				int result = imageChooser.showOpenDialog(new JFrame());
				//If the user has chosen an image
				if (result == JFileChooser.APPROVE_OPTION) {
					//Get the chosen file
					File selectedFile = imageChooser.getSelectedFile();
					try 
					{
						//Read in the chosen file
						images.put(triggerKeyIdentifier, ImageIO.read(selectedFile));
						//Display the selected file to the user
						displayImagePath.setText(selectedFile.getName());
						pack();
					} 
					catch (IOException ex) 
					{
						JOptionPane.showMessageDialog(null, "There was a problem loading the image",
								"error", JOptionPane.PLAIN_MESSAGE);
					}
				}
			}
		});

		return createLabelComponentPanel(chooseImage, chooseImagePanel);
	}

	/**
	 * Add the save and cancel buttons to the window.
	 */
	private void addButtons() {
		//Create a panel and buttons
		JPanel buttonGroup = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		save = new JButton("Save");
		cancel = new JButton("Cancel");
		//Add action listener to the buttons
		save.addActionListener(this);
		cancel.addActionListener(this);

		//Add the buttons to the panel
		buttonGroup.add(save);
		buttonGroup.add(cancel);

		//Add the buttons to the bottom of the winow
		window.add(buttonGroup, BorderLayout.SOUTH);
	}

	/**
	 * React to action events.
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		//The save button has been pressed
		if (e.getSource() == save) {
			//Only proceed if the form is valid
			if (validateForm()) {
				//Get the name of the keymap, trim excess white space
				String keymapName = name.getText().trim();
				//Attempt to create a new keymap
				if (presenter.createNewKeyMap(keymapName, parent)) {
					//Open the main mapping window
					presenter.openMainGUI(keymapName, false);
					//Assign the trigger keys
					assignTriggerKeys(keymapName);
					//Load the newly created keymap
					presenter.loadKeyMap(keymapName);
					//Close the popup window
					dispose();
				} else {
					//If the keymap creation fails, the keymap name must be taken
					JOptionPane.showMessageDialog(this, "A keymap by that name already exists. Keymap names must be unique.", "error", JOptionPane.PLAIN_MESSAGE);
				}
			}
			//The cancel button has been presses
		} else if (e.getSource() == cancel) {
			//Close the popup window
			dispose();
		}
	}

	/**
	 * Assign the trigger keys.
	 * @param keymapName	the name of keymap being created
	 */
	private void assignTriggerKeys(String keymapName) {
		//Assign the first trigger key
		if (oneTrigger.isSelected() || twoTrigger.isSelected() || threeTrigger.isSelected()) {
			String displayType = (String) triggerComboOne.getSelectedItem();
			//Set the trigger key
			presenter.setTrigger(keyStringOne, keymapName);

			//Assign a text overlay if one has been selected
			if (displayType.equals("Text Overlay")) {
				presenter.setTextOverlay(keyStringOne, keymapName, textOverlayOne.getText());
				//Assign a image overlay if one has been selected	
			} else if (displayType.equals("Image Overlay")) {
				presenter.setKeyImage(keyStringOne, keymapName, images.get("One"));
			}
		} 
		//Assign the second trigger key
		if (twoTrigger.isSelected() || threeTrigger.isSelected()) {
			String displayType = (String) triggerComboTwo.getSelectedItem();
			//Set the trigger key
			presenter.setTrigger(keyStringTwo, keymapName);

			//Assign a text overlay if one has been selected
			if (displayType.equals("Text Overlay")) {
				presenter.setTextOverlay(keyStringTwo, keymapName, textOverlayTwo.getText());
				//Assign a image overlay if one has been selected
			} else if (displayType.equals("Image Overlay")) {
				presenter.setKeyImage(keyStringTwo, keymapName, images.get("Two"));
			}
		} 
		//Assign the thrid trigger key
		if (threeTrigger.isSelected()) {
			String displayType = (String) triggerComboThree.getSelectedItem();
			//Set the trigger key
			presenter.setTrigger(keyStringThree, keymapName);

			//Assign a text overlay if one has been selected
			if (displayType.equals("Text Overlay")) {
				presenter.setTextOverlay(keyStringThree, keymapName, textOverlayThree.getText());
				//Assign a IMAGE overlay if one has been selected
			} else if (displayType.equals("Image Overlay")) {
				presenter.setKeyImage(keyStringTwo, keymapName, images.get("Three"));
			}
		}
	}

	/**
	 * Validate the form.
	 * @return	returns <code>true</code> if the form is valid, 
	 * 			<code>false</code> otherwise
	 */
	private boolean validateForm() {
		//Make sure the user has entered a name for the keymap
		if (name.getText().equals("")) {
			JOptionPane.showMessageDialog(this, "Please enter a name for the keymap.", "error", JOptionPane.PLAIN_MESSAGE);
			return false;
		}
		//Validate forms inputs based on the number of trigger keys selected
		//Make sure the first trigger key is valid
		if (oneTrigger.isSelected()) {
			return validateTriggerOne();
		}
		//Make sure the first and second trigger key are valid
		if (twoTrigger.isSelected()) {
			return (validateTriggerOne() && validateTriggerTwo());
		} 
		//Make sure all three trigger keys are valis
		if (threeTrigger.isSelected()) {
			return (validateTriggerOne() && validateTriggerTwo() && validateTriggerThree());
		}
		return true;
	}

	/**
	 * Validate the input for the first trigger key.
	 * @return	returns <code>true</code> if the first trigger key 
	 * 			input is valid, <code>false</code> otherwise
	 */
	private boolean validateTriggerOne() {
		String displayType = (String) triggerComboOne.getSelectedItem();

		//Make sure a trigger key has been selected
		if (keyStringOne == null) {
			JOptionPane.showMessageDialog(this, "You have not selected the first trigger key.", "error", JOptionPane.PLAIN_MESSAGE);
			return false;
		}
		//Make sure that an image has been chosen if the image overlay display style has been selected
		if (displayType.equals("Image Overlay") && !images.containsKey("One")) {
			JOptionPane.showMessageDialog(this, "You have not selected an image for the first trigger key.", "error", JOptionPane.PLAIN_MESSAGE);
			return false;
		} 

		return true;
	}

	/**
	 * Validate the input for the second trigger key.
	 * @return	returns <code>true</code> if the second trigger key 
	 * 			input is valid, <code>false</code> otherwise
	 */
	private boolean validateTriggerTwo() {
		String displayType = (String) triggerComboTwo.getSelectedItem();

		//Make sure a trigger key has been selected
		if (keyStringTwo == null) {
			JOptionPane.showMessageDialog(this, "You have not selected the second trigger key.", "error", JOptionPane.PLAIN_MESSAGE);
			return false;
		}
		//Make sure that an image has been chosen if the image overlay display style has been selected
		if (displayType.equals("Image Overlay") && !images.containsKey("Two")) {
			JOptionPane.showMessageDialog(this, "You have not selected an image for the second trigger key.", "error", JOptionPane.PLAIN_MESSAGE);
			return false;
		}
		//Do not allow the same key to be selected multiple times
		if (keyStringTwo.equals(keyStringOne)) {
			JOptionPane.showMessageDialog(this, "You have selected the same key multiple times.", "error", JOptionPane.PLAIN_MESSAGE);
			return false;
		}

		return true;
	}

	/**
	 * Validate the input for the third trigger key.
	 * @return	returns <code>true</code> if the third trigger key 
	 * 			input is valid, <code>false</code> otherwise
	 */
	private boolean validateTriggerThree() {
		String displayType = (String) triggerComboThree.getSelectedItem();

		//Make sure a trigger key has been selected
		if (keyStringThree == null) {
			JOptionPane.showMessageDialog(this, "You have not selected the third trigger key.", "error", JOptionPane.PLAIN_MESSAGE);
			return false;
		}
		//Make sure that an image has been chosen if the image overlay display style has been selected
		if (displayType.equals("Image Overlay") && !images.containsKey("Third")) {
			JOptionPane.showMessageDialog(this, "You have not selected an image for the third trigger key.", "error", JOptionPane.PLAIN_MESSAGE);
			return false;
		} 
		//Do not allow the same key to be selected multiple times
		if (keyStringThree.equals(keyStringOne) || keyStringThree.equals(keyStringTwo)) {
			JOptionPane.showMessageDialog(this, "You have selected the same key multiple times.", "error", JOptionPane.PLAIN_MESSAGE);
			return false;
		}

		return true;
	}

	@Override
	public void keyPressed(KeyEvent arg0) {
		//Method unused but required for interface
	}

	/**
	 * Listen for key presses. Used to assign the trigger keys based 
	 * upon user key press
	 */
	@Override
	public void keyReleased(KeyEvent e) {
		//Get the keycode of the key pressed
		int keyCode = e.getKeyCode();
		//Get the name of the key
		String keyText = KeyEvent.getKeyText(keyCode);

		String locationString = "";

		//Get the location of the key
		int location = e.getKeyLocation();
		if (location == KeyEvent.KEY_LOCATION_STANDARD) {
			locationString += "standard";
		} else if (location == KeyEvent.KEY_LOCATION_LEFT) {
			locationString += "left";
		} else if (location == KeyEvent.KEY_LOCATION_RIGHT) {
			locationString += "right";
		} else if (location == KeyEvent.KEY_LOCATION_NUMPAD) {
			locationString += "numpad";
		} else { // (location == KeyEvent.KEY_LOCATION_UNKNOWN)
			locationString += "unknown";
		}
		//The string that uniquely identifies the chosen key
		keyText += " " + locationString;

		//The string to be display to the user
		String mappingString = "Key Selected: " + keyText;

		//Set the trigger key instance variables and display
		//the choice to users
		if (e.getSource() == triggerKeyOne) {
			triggerKeyOne.setText(mappingString);
			keyStringOne = keyText;
		} else if (e.getSource() == triggerKeyTwo) {
			triggerKeyTwo.setText(mappingString);
			keyStringTwo = keyText;
		} else if (e.getSource() == triggerKeyThree) {
			triggerKeyThree.setText(mappingString);
			keyStringThree = keyText;
		}
	}

	@Override
	public void keyTyped(KeyEvent e) {
		//Method unused but required for interface
	}
}
