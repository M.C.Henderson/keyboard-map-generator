package mapping;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 * Advanced mapping window that allows users to define key mappings or randomise keys. 
 * Key mappings can be defined by pressing keys or imputing unicode directly.
 * Display options are image, text or default Unicode.
 * @author Matthew Henderson - University of Glasgow Masters Project 2018
 * 
 */
public class AdvancedMappingPopup extends JFrame implements ActionListener, KeyListener {
	
	//Layout Components
	private JTextField mapping, keyText, unicodeInput;
	private JLabel overlayPicker, mappingLabel, keyTextLabel, chosenKey, chosenKeyLabel, currentKeyDisplay, currentKeyDisplayLabel;
	private JLabel currentKeyMapping, currentKeyMappingLabel, unicodePrompt, entryType,  unicodeLabel, chooseImage, displayImagePath;
	private String key, keymap, displayType, unicode, currentMapping;
	private JFileChooser imageChooser;
	private JButton save, cancel, chooseImageButton, randomise, reset;
	private Presenter presenter;
	private JComboBox<String> overlayCombo, mappingCombo;
	private GridBagConstraints layoutConstraints;
	private JPanel window, currentMappingInfo, newMapping, chooseImagePanel, unicodePanel, infoGroup;
	
	//Key press info
	private int keyPressesToIgnore;
	//Flags for input style
	private String overlayStyle, entryStyle;
	//The image uploaded by the user
	private BufferedImage img;

	/**
	 * Create the mapping popup window.
	 * @param key				the unique identifier of the key that will be mapped 
	 * @param keymap			the name of the keymap the key belongs to 
	 * @param displayType		the current display type of the key
	 * @param currentMapping	the current mapping of the key
	 * @param presenter			the presenter in the mvp architecture
	 */
	public AdvancedMappingPopup(String key, String keymap, String displayType, String currentMapping, Presenter presenter) {
		this.key = key;
		this.keymap = keymap;
		this.displayType = displayType;
		this.currentMapping = currentMapping;
		this.presenter = presenter;
		
		//Key press information. Set to default
		unicode = "";
		keyPressesToIgnore = 0;
		
		//Set up the JFrame
		setSize(500, 500);
		setLayoutComponents();
		setInitialDisplay();
		setTitle("Advanced Mapping Selection");
		pack();
	}
	
	/**
	 * Set the layout components of the window.
	 */
	private void setLayoutComponents() {
		//Create the main JPanel
		window = new JPanel(new BorderLayout());
		layoutConstraints = new GridBagConstraints();
		
		//Add all the components to the main window panel
		addCurrentMappingInfo();
		addNewMappingGroup();
		addButtons();
		
		//Add the window panel to the frame
		this.add(window);
	}
	
	/**
	 * Sets the initial selection of the combo boxes.
	 */
	private void setInitialDisplay() {
		//Set the initial display of the combo boxes
		//based upon the current display style of the key
		if (displayType.equals("Text Overlay"))  {
			//set to text overlay
			overlayCombo.setSelectedItem("Text Overlay");
		} else if (displayType.equals("Image")) {
			//Set to image
			overlayCombo.setSelectedItem("Image");
		} 
		
		//Update instance variables to reflect this
		this.overlayStyle = (String)overlayCombo.getSelectedItem();;
		this.entryStyle = (String)mappingCombo.getSelectedItem();
	}
	
	/**
	 * Adds and activates all the components required to 
	 * define a mapping.
	 */
	private void addNewMappingGroup() {
		//Create a panel for the compenents to set a new mapping
		newMapping = new JPanel(new GridBagLayout());
		
		//Create all the components required to choose a mapping
		chooseMappingComponents();
		chooseDisplayComponents();
		textOverlayComponents();
		imageOverlayComponents();
		 	
    	//Add all the components to the main window
		//Add entry type components
    	JPanel panel;
    	newMapping.setBorder(BorderFactory.createTitledBorder("New Mapping"));
		layoutConstraints.insets = new Insets(0, 0, 10, 0);
		layoutConstraints.anchor = GridBagConstraints.LINE_END;
		layoutConstraints.gridx = 0;
		layoutConstraints.gridy = 0;
		newMapping.add(entryType, layoutConstraints);
		layoutConstraints.anchor = GridBagConstraints.LINE_START;
		layoutConstraints.gridx = 1;
		layoutConstraints.gridy = 0;
		panel = new JPanel();
		panel.add(mappingCombo);
		newMapping.add(panel, layoutConstraints);
		//Add components for choosing mappings
		layoutConstraints.anchor = GridBagConstraints.LINE_END;
		layoutConstraints.gridx = 0;
		layoutConstraints.gridy = 1;
		newMapping.add(mappingLabel, layoutConstraints);
		newMapping.add(unicodeLabel, layoutConstraints);
		layoutConstraints.anchor = GridBagConstraints.LINE_START;
		layoutConstraints.gridx = 1;
		layoutConstraints.gridy = 1;
		panel = new JPanel();
		panel.add(mapping);
		newMapping.add(panel, layoutConstraints);
		newMapping.add(unicodePanel, layoutConstraints);
		//Add components for selecting display style
		layoutConstraints.anchor = GridBagConstraints.LINE_END;
		layoutConstraints.gridx = 0;
		layoutConstraints.gridy = 2;
		newMapping.add(overlayPicker, layoutConstraints);
		layoutConstraints.anchor = GridBagConstraints.LINE_START;
		layoutConstraints.gridx = 1;
		layoutConstraints.gridy = 2;
		panel = new JPanel();
		panel.add(overlayCombo);
		newMapping.add(panel, layoutConstraints);
		//Add components for selecting overlay
		layoutConstraints.anchor = GridBagConstraints.LINE_END;
		layoutConstraints.gridx = 0;
		layoutConstraints.gridy = 3;
		newMapping.add(keyTextLabel, layoutConstraints);
		newMapping.add(chooseImage, layoutConstraints);
		layoutConstraints.anchor = GridBagConstraints.LINE_START;
		layoutConstraints.gridx = 1;
		layoutConstraints.gridy = 3;
		panel = new JPanel();
		panel.add(keyText);
		newMapping.add(panel, layoutConstraints);
		newMapping.add(chooseImagePanel, layoutConstraints);
		
		window.add(newMapping, BorderLayout.CENTER);
	}
	
	/**
	 * Adds and activates the components required for choosing an image overlay
	 */
	private void imageOverlayComponents() {
		//Filter the file chooser to only show image files
		FileFilter imageFilter = new FileNameExtensionFilter("Image files", ImageIO.getReaderFileSuffixes());
		//Create the file chooser
    	imageChooser = new JFileChooser();
    	//Attach filter to JFileChooser object
    	imageChooser.setFileFilter(imageFilter);
    	//Add file chooser button and labels
    	chooseImage = new JLabel("Choose Image: ");
    	chooseImageButton = new JButton("Browse...");
    	//Label to display the selected image
    	displayImagePath = new JLabel();
    	//Container for image components
    	chooseImagePanel = new JPanel();
    	chooseImagePanel.add(chooseImageButton); 
    	chooseImagePanel.add(displayImagePath);
    	
    	//Make image selection initially invisible
    	chooseImage.setVisible(false);
    	chooseImageButton.setVisible(false);
    	
    	//Add action listener to choose image button
    	chooseImageButton.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e)
		    {
		    	//Open the file chooser
		    	int result = imageChooser.showOpenDialog(new JFrame());
		    	//If the user has chosen an image
		    	if (result == JFileChooser.APPROVE_OPTION) {
		    		//Get the chosen file
		    	    File selectedFile = imageChooser.getSelectedFile();
		    	    try 
		    	    {
		    	    	//Read in the chosen file
		    	        img = ImageIO.read(selectedFile);
		    	        //Display the selected file to the user
		    	        displayImagePath.setText(selectedFile.getName());
		    	        pack();
		    	    } 
		    	    catch (IOException ex) 
		    	    {
		    	    	JOptionPane.showMessageDialog(null, "There was a problem loading the image",
								"error", JOptionPane.PLAIN_MESSAGE);
		    	    }
		    	}
		    }
		});
	}
	
	/**
	 * Adds and activates the components required for choosing a text overlay
	 */
	private void textOverlayComponents() {
		//Set the text overlay selection and make it initially invisible
		keyTextLabel = new JLabel("Key Text Overlay: ");
		keyText = new JTextField(18);
		keyTextLabel.setVisible(false);
    	keyText.setVisible(false);
	}
	
	/**
	 * Adds the components to allow the user to select a display type
	 */
	private void chooseDisplayComponents() {
		//The options for the combo box
		String[] overlayOptions = {"Default Unicode Representation", "Image", "Text Overlay"};
		
		//Create the combo box and label
		overlayPicker = new JLabel("Choose key display: ");
		overlayCombo = new JComboBox<String>(overlayOptions);
		//Adds an action listener to the combo box
		overlayCombo.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e)
		    {
		    	@SuppressWarnings("unchecked")
		    	//Get the user selection
				JComboBox<String> cb = (JComboBox<String>)e.getSource();
		        overlayStyle = (String)cb.getSelectedItem();
		        
		        //If the user has chosen default display, hide all options
		        //for display selection
		        if (overlayStyle.equals("Default Unicode Representation")) {
		        	keyTextLabel.setVisible(false);
		        	keyText.setVisible(false);
		        	chooseImage.setVisible(false);
		        	chooseImageButton.setVisible(false);
		        //Image overlay has been chosen, activate components
		        //for choosing images
		        } else if (overlayStyle.equals("Image")) {
		        	keyTextLabel.setVisible(false);
		        	keyText.setVisible(false);
		        	chooseImage.setVisible(true);
		        	chooseImageButton.setVisible(true);
		        //Test overlay has been chosen, activate components
		        //for defining text
		        } else {
		        	keyTextLabel.setVisible(true);
		        	keyText.setVisible(true);
		        	chooseImage.setVisible(false);
		        	chooseImageButton.setVisible(false);
		        }
		        pack();
		    }
		});
	}
	
	/**
	 * Adds the components to allow a user to choose a mapping
	 */
	private void chooseMappingComponents() {
		//The options in the combo box
		String[] mappingOptions = {"Typing Key", "Entering Unicode"};
		
		//Set the components to choose mapping input style
		entryType = new JLabel("Choose Mapping By: ");
		mappingCombo = new JComboBox<String>(mappingOptions);
		//Make the width of the combo box as wide as the other JCombo
		mappingCombo.setPrototypeDisplayValue("Default Unicode Representation");
		
		//Components for manual Unicode input
		//Make initially invisible
		unicodeLabel = new JLabel("Enter Unicode: ");
		unicodeLabel.setVisible(false);
		unicodePrompt = new JLabel("  \\u");
		unicodePrompt.setVisible(false);
		unicodeInput = new JTextField(16);
		unicodeInput.setVisible(false);
		//Combine components into a panel
		unicodePanel = new JPanel();
		unicodePanel.add(unicodePrompt);
		unicodePanel.add(unicodeInput);
		
		//Components for typed key input 
		mappingLabel = new JLabel("Mapping (Type key): ");
		mapping = new JTextField(18);
		//Add listener so key presses can be detected
		mapping.addKeyListener(this);
		
		//Add action listener to combo box
		mappingCombo.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e)
		    {
		    	@SuppressWarnings("unchecked")
		    	//Get the users selection
				JComboBox<String> cb = (JComboBox<String>)e.getSource();
		        entryStyle = (String)cb.getSelectedItem();
		        
		        //User has chosen to type the key
		        if (entryStyle.equals("Typing Key")) {
		        	//Only show the components need for typed key entry
		    		unicodeLabel.setVisible(false);
		    		unicodePrompt.setVisible(false);
		    		unicodeInput.setVisible(false);
		    		mappingLabel.setVisible(true);
		    		mapping.setVisible(true);
		    	//User has chosen Unicode entry
		        } else if (entryStyle.equals("Entering Unicode")) {
		        	//Only show the components need for Unicode entry
		        	unicodeLabel.setVisible(true);
		    		unicodePrompt.setVisible(true);
		    		unicodeInput.setVisible(true);
		    		mappingLabel.setVisible(false);
		    		mapping.setVisible(false);
		        }
		        pack();
		    }
		});
	}
	
	/**
	 * Display the current key mapping to the user.
	 */
	private void addCurrentMappingInfo() {
		//Create the group in the window to show current mapping info
		infoGroup = new JPanel(new GridBagLayout());
		infoGroup.setBorder(BorderFactory.createTitledBorder("Key Info"));
		//Set the layout of the current mapping display
		GridLayout gl = new GridLayout(3,2);
		gl.setVgap(5);
		gl.setHgap(5);
		currentMappingInfo = new JPanel(gl);
		
		//Create the label components that will display the info
		chosenKeyLabel = new JLabel("Key: ");
		chosenKey = new JLabel(key);
		currentKeyDisplayLabel = new JLabel("Display Type: ");
		currentKeyDisplay = new JLabel(displayType);
		currentKeyMappingLabel = new JLabel("Current Mapping: ");
		currentKeyMapping = new JLabel(currentMapping);
		
		//create a button to delete the current mapping
		reset = new JButton("Reset Key");
		reset.addActionListener(this);

		//Group label in the same panel as display
		//Currently chosen key
		currentMappingInfo.add(chosenKeyLabel);
		chosenKeyLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		currentMappingInfo.add(chosenKey);
		//Current mapping for the key
		currentMappingInfo.add(currentKeyMappingLabel);
		currentKeyMappingLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		currentMappingInfo.add(currentKeyMapping);
		//Current display type
		currentMappingInfo.add(currentKeyDisplayLabel);
		currentKeyDisplayLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		currentMappingInfo.add(currentKeyDisplay);
		
		//Add the components to the panel
		GridBagConstraints tempConstraints = new GridBagConstraints();
		infoGroup.add(currentMappingInfo, tempConstraints);
		tempConstraints.gridx ++;
		tempConstraints.insets = new Insets(5, 0, 0, 0);
		infoGroup.add(reset, tempConstraints);
		
		//Add the panel to the window
		window.add(infoGroup, BorderLayout.NORTH);
	}
	
	/**
	 * Adds the buttons to the button of the window.
	 */
	private void addButtons() {
		//Add save and cancel buttons to the right and randomise to the left
		JPanel saveCancel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		JPanel windowButtons = new JPanel(new GridLayout(1, 2));
		JPanel rand = new JPanel(new FlowLayout(FlowLayout.LEFT));
		
		//Create the buttons and add action listeners
		save = new JButton("Save");
		cancel = new JButton("Cancel");
		randomise = new JButton("Randomise");
		save.addActionListener(this);
		cancel.addActionListener(this);
		randomise.addActionListener(this);
		
		//Add the components to the window
		saveCancel.add(save);
		saveCancel.add(cancel);
		rand.add(randomise);
		windowButtons.add(rand);
		windowButtons.add(saveCancel);
		window.add(windowButtons, BorderLayout.SOUTH);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		//The save button has been pressed
		if (e.getSource() == save) {
			//Check the form is valid
			if (validateForm()) {
				//set the mapping, reload the keymap and close the window
				setMapping();
				presenter.loadKeyMap(keymap);
				dispose();
			}
		//The randomise key has been pressed
		} else if (e.getSource() == randomise) {
			//randomise the key and close the window
			setRandomised();
			dispose();
		//Cancel has been pressed
		} else if (e.getSource() == cancel) {
			//close the window
			dispose();
		//reset has been pressed
		} else if (e.getSource() == reset) {
			currentKeyDisplay.setText("None");
			currentKeyMapping.setText("None");
			//Reset the current key display and mapping
			presenter.resetKey(key, keymap);
		}
	}
	
	/**
	 * Sets the key mapping based on user selection.
	 */
	private void setMapping() {
		//Check what the input style is
		if(entryStyle.equals("Entering Unicode")) {
			//Set mapping based on users unicode input
			String uniHex = unicodeInput.getText();
			
			//Pad with zeros if Unicode is shorter than 4
			if (uniHex.length() < 4 && !uniHex.equals("")) { 
				uniHex = "0000".substring(uniHex.length()) + uniHex;
			}
			
			//If a unicode hex has not been set, leave unicode argument blank
			//Otherwise add the unicode string identifier
			String uniString = uniHex.equals("") ? "" : "\\u" + uniHex;
			presenter.setKeyMapping(key, keymap, uniString);
		} else {
			//Set the mapping based on the key typed
			presenter.setKeyMapping(key, keymap, unicode);
		}
		
		//Set the display content based on user selection for display type
		if (overlayStyle.equals("Image")) {
			//Set the image overlay
			presenter.setKeyImage(key, keymap, img);
		} else if (overlayStyle.equals("Text Overlay")) {
			//Set the text overlay
			presenter.setTextOverlay(key, keymap, keyText.getText());
		} else {
			//Default display has been chosen. Remove any previous display
			presenter.removeDisplay(key, keymap);
		}
	}
	
	/**
	 * Sets the key as randomised.
	 */
	private void setRandomised() {
		presenter.setAsRandom(key, keymap);
		presenter.loadKeyMap(keymap);
	}
	
	/**
	 * Validates the form before the model is updated.
	 * @return	returns <code>true</code> if the form is valid,   
	 * 			<code>false</code> otherwise
	 */
	public boolean validateForm() {
		//Ensure the user has chosen a key if the user has picked the typing key option
		if (entryStyle.equals("Typing Key") && overlayStyle.equals("Default Unicode Representation")) {
			//Not all keys produce a unicode character. Ensure that the chosen one does
			if (unicode == null || unicode.equals("")) {
				//Display error message and return false
				JOptionPane.showMessageDialog(this, "You must choose either a mapping, an image or a text overlay.\nRemember that your"
						+ " chosen key mapping must produce of Unicode character.", "error", JOptionPane.PLAIN_MESSAGE);
				return false;
			}
		//Ensure the user has entered a valid unicode hex string if the entering unicode option is selected
		} else if (entryStyle.equals("Entering Unicode") && overlayStyle.equals("Default Unicode Representation")) {
			//Check the unicode is valid
			if (!UnicodeConverter.isUnicodeHexString("\\u" + unicodeInput.getText())) {
				//Display error message and return false
				JOptionPane.showMessageDialog(this, "You have not entered a valid unicode hex string.", "error", JOptionPane.PLAIN_MESSAGE);
				return false;
			}
		}
		
		//Check that the user has selected an image if they have chosen the image overlay style
		if (overlayStyle.equals("Image")) {
			if (img == null) {
				//Display error message and return false
				JOptionPane.showMessageDialog(this, "You have not selected an image.", "error", JOptionPane.PLAIN_MESSAGE);
				return false;
			}
		}
		//form is valid
		return true;
	}

	@Override
	public void keyPressed(KeyEvent e) {
		//unused method required for interface
	}

	/**
	 * Responds to key presses inside the type key text box. 
	 * Sets the chosen keys and displays it to the user.
	 * @param e	the KeyEvent object
	 */
	@Override
	public void keyReleased(KeyEvent e) {
		//Information about key pressed
		int mappingKeyCode = e.getKeyCode();
		int modifiersEx = e.getModifiersEx();
		String keyText = KeyEvent.getKeyText(mappingKeyCode);
		int mappingKeyChar = e.getKeyChar();
		
		String mappingString = "Key Selected: ";
		
		//Key presses must be ignored so modifiers don't overwrite modified keys when released
		if (keyPressesToIgnore > 0) {
			keyPressesToIgnore --;
			return;
		//Modified keys
		} else 	if (modifiersEx != 0) {
			mappingString += KeyEvent.getModifiersExText(modifiersEx) + " + " + keyText;
			mapping.setText(mappingString);
			
			//2 modifiers to ignore
			if (modifiersEx == 192 || modifiersEx == 640 || modifiersEx == 576) {
				keyPressesToIgnore = 2;
			//1 modifier to ignore
			} else if (modifiersEx == 128 || modifiersEx == 512 || modifiersEx == 64) {
				keyPressesToIgnore = 1;
			}
		//Unmodified keys
		} else {
			mappingString += keyText;
			mapping.setText(mappingString);
		}
		
		//Convert the generated key char to hex
		String keyCharUni = UnicodeConverter.convertIntToHex(mappingKeyChar);
		
		//Character does not have a unicode representation, set to an empty string
		if (keyCharUni.equals("\\uffff")) {
			unicode = "";
			mapping.setText(mappingString + " (Invalid)");
		//Set the unicode representation
		} else {
			unicode = keyCharUni; 
		}
	}

	@Override
	public void keyTyped(KeyEvent e) {
		//Unused method required for interface
	}
}
