package mapping;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 * Window that allows the user to choose whether to load a pre-existing XML file
 * to edit or to create a new keymap file. This is the first window that is shown 
 * when the program is run.
 * @author Matthew Henderson - University of Glasgow Masters Project 2018
 */
public class LoadXmlPopup extends JFrame implements ActionListener {
	
	JButton loadKm, newKm;
	JLabel description;
	JPanel content, window;
	JFileChooser fileChooser;
	Presenter presenter;

	/**
	 * Class constructor for the popup. Sets the layout and meta data.
	 * @param presenter	the presenter object that will be used to update the model
	 */
	public LoadXmlPopup(Presenter presenter) {
		this.presenter = presenter;
		
		setTitle("Keyboard Mapping Tool");
		setLayoutComponents();
		pack();
	}
	
	/**
	 * Adds all components to the frame.
	 */
	private void setLayoutComponents() {
		//Text that describes the window
		description = new JLabel("<html><div style='text-align: center;'>Welcome to the keyboard mapping tool.<br/>"
				+ "Please load in a keymap XML file for editing or create a new keymap from scratch.</div></html>", SwingConstants.CENTER);
		
		//Create buttons and add listeners
		loadKm = new JButton("Load existing keymap file");
		newKm = new JButton("Create new keymap file");
		loadKm.addActionListener(this);
		newKm.addActionListener(this);
		
		//Set up the file chooser
		fileChooser = new JFileChooser();
		FileFilter filter = new FileNameExtensionFilter("XML files", "xml");
		fileChooser.setFileFilter(filter);
		
		//Add the components to a panel
		content = new JPanel(); 
		content.add(loadKm);
		content.add(newKm);
		
		//Add to the main window panel
		window = new JPanel(new GridLayout(2, 1));
		window.add(description);
		window.add(content);
		
		//Add the window panel to the frame
		this.add(window);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		//Load button has been pressed
		if (e.getSource() == loadKm) {
			//Loads the main GUI displaying the keymaps in the selected file
			int result = fileChooser.showOpenDialog(new JFrame());
			//If valid file, get the path and load the keymaps
	    	if (result == JFileChooser.APPROVE_OPTION) {
	    	    File selectedFile = fileChooser.getSelectedFile();
    	    	presenter.loadExistingFile(selectedFile.getAbsolutePath());
	    	    dispose();
	    	}
		//New button has been pressed
		} else if (e.getSource() == newKm) {
			//Open the main GUI without any pre-existing keymaps
			presenter.openNewKeyMapWindow(null);
			dispose();
		}
	}
}
