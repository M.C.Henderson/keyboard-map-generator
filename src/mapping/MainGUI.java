package mapping;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.dnd.DnDConstants;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.HashMap;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTree;
import javax.swing.SwingUtilities;
import javax.swing.TransferHandler;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

/**
 * The main graphical interface of the application. Contains the virtual keyboard 
 * which displays mappings to the user. From this window mappings can be added, 
 * removed or edited. The mappings XML can also be generated from this window.
 * @author Matthew Henderson - University of Glasgow Masters Project 2018
 *
 */
public class MainGUI extends JFrame implements ActionListener, TreeSelectionListener {	
	protected HashMap<JButton, String> buttonKeyPairings;
	protected JPanel keyboardArea;
	private HashMap<JButton, String> buttonDefaultDisplay;
	private Presenter presenter;
	
	private JPanel window;
	private String currentKeymap;
	private JTree tree;
	private JTabbedPane tabbedPane;
	private JScrollPane treeView;
	private JPanel buttonPanel, buttonGrid;
	private JPopupMenu keymapOptionsMenu;
	private JMenuItem newKeymap, deleteKeymap, rename;
	private DefaultMutableTreeNode selectedNode;
	private JButton xml, reset, load;
	private JFileChooser chooser, fileChooser;

	/**
	 * Creates and shows the main graphical interface
	 * @param presenter	the presenter in the mvp model
	 * @param keymap	the initial keymap to be displayed
	 */
	public MainGUI(Presenter presenter, String keymap) {
		this.presenter = presenter;
		this.currentKeymap = keymap;
		//Set up the JFrame
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setTitle("AR Keyboard Mapping");
		//Make the window fill the screen
		setExtendedState(JFrame.MAXIMIZED_BOTH);
		keyboardArea = new JPanel();
	}
	
	/**
	 * Builds the graphical user interface. 
	 * This method must be called after the keyboard components 
	 * have been added to the keyobardArea panel otherwise the 
	 * virtual keyboard will not initially be displayed.
	 */
	protected void buildKeyboard() {
		initDisplay();
		addEmojiPanel();
		addKeymapTree();
		addListeners();
		buildPopups();
		storeDefaultDisplays();
	}
	
	/**
	 * Takes the initially assigned button text and stores it
	 * so that that the initial display can be restored if new
	 * keymaps are created.
	 */
	private void storeDefaultDisplays() {
		buttonDefaultDisplay = new HashMap<JButton, String>(); 
		//Get the display text assigned to the buttons so that it can be reloaded
		for (JButton button : buttonKeyPairings.keySet()) {
		    buttonDefaultDisplay.put(button, button.getText());
		}
	}
	
	/**
	 * Creates the popup that will appear when the user right 
	 * clicks on the keymap tree. This popup allows users to 
	 * create new keymaps or delete keymaps.
	 */
	private void buildPopups() {
		//Create the popup menu and menu items
		keymapOptionsMenu = new JPopupMenu();
		newKeymap = new JMenuItem("New Child Keymap");
		rename = new JMenuItem("Rename Keymap");
		deleteKeymap = new JMenuItem("Delete Keymap");
		//Add menu items to the menu
		keymapOptionsMenu.add(newKeymap);
		keymapOptionsMenu.add(rename);
		keymapOptionsMenu.add(deleteKeymap);
		
		//Add new keymap action listener
		newKeymap.addActionListener(new ActionListener() {

		    public void actionPerformed(ActionEvent e)
		    {
		    	//Get the tree node that was right clicked on
		    	String keymap = (String) selectedNode.getUserObject();
				
		    	//Check if the user has clicked on the keymap container node
		    	if (keymap.equals("Keymaps")) {
		    		//Add the new keymap to the top level of the hierarchy
		    		presenter.openNewKeyMapWindow(null);
		    	} else {
		    		//Add the new keymap as a child of the keymap that was
		    		//clicked on
		    		presenter.openNewKeyMapWindow(keymap);
		    	}
		    }
		});
		
		//Add the rename action listener
		rename.addActionListener(new ActionListener() {

		    public void actionPerformed(ActionEvent e)
		    {
		    	//Get the tree node that was right clicked on
		    	String keymap = (String) selectedNode.getUserObject();
		    	
		    	//Make sure the chosen node is a map
		    	if (!keymap.equals("Keymaps")) {
			    	//Get the user to pick a new name
			    	String newName = JOptionPane.showInputDialog(null, "Enter the new keymap name.");
					
			    	//Check the name is valid and is not already in use by another keymap
			    	if (newName == null ) {
			    		//Do nothing
			    	}
			    	else if (!newName.equals("") && presenter.renameKeymap(keymap, newName)) {
			    		//Change the JTree to reflect the change
			    		selectedNode.setUserObject(newName);
			    	    ((DefaultTreeModel) tree.getModel()).nodeChanged(selectedNode);
			    	} else if (!keymap.equals("Keymaps")) {
			    		//Tell the user the rename was not successful
			    		JOptionPane.showMessageDialog(null, "Could not rename keymap. Remember keymap names must be unique.", "error", JOptionPane.PLAIN_MESSAGE);
			    	}
		    	}
		    }
		}); 

		//Add the delete keymap action listener
		deleteKeymap.addActionListener(new ActionListener() {

		    public void actionPerformed(ActionEvent e)
		    {
		    	//Get the tree node that was right clicked on
		    	String keymap = (String) selectedNode.getUserObject();
				DefaultTreeModel model = (DefaultTreeModel) tree.getModel();
				
				//Don't let the user remove the container for the keymaps node
		    	if (!keymap.equals("Keymaps")) {
		    		if (currentKeymap == null) {
		    			//No need to change current keymap
		    		}
		    		else if (currentKeymap.equals(keymap)) {
		    			//The current keymap will be deleted
		    			//Deselect the keymap
		    			currentKeymap = null;
		    			setDefaultDisplay();
		    		}
		    		//Delete the keymap
		    		presenter.deleteKeymap(keymap);
		    		//Remove the keymap from the display tree
		    		model.removeNodeFromParent(selectedNode);
		    	}
		    }
		}); 
	}
	
	/**
	 * Create the main body of the display
	 */
	private void initDisplay() {		
		window = new JPanel(new BorderLayout());
		//Add the virtual keyboard to tge window
		window.add(keyboardArea, BorderLayout.CENTER);
		
		//Create the buttons
		xml = new JButton("Save");
		load = new JButton("Load");
		reset = new JButton("Reset Display");
		//Add action listener to the buttons
		reset.addActionListener(this);
		xml.addActionListener(this);
		load.addActionListener(this);
		
		//Group the buttons together in a panel
		buttonPanel = new JPanel(new BorderLayout());
		GridLayout gl = new GridLayout(3,1);
		//Space the buttons by 10 pixels
		gl.setVgap(10);
		//Create a grid to stack buttons horizontally
		buttonGrid = new JPanel(gl);
		//Add the button panel to the right side of the window
		window.add(buttonPanel, BorderLayout.EAST);
		//Add the button to the grid
		buttonGrid.add(xml);
		buttonGrid.add(load);
		buttonGrid.add(reset);
		//Add grid to the button panel 
		buttonPanel.add(buttonGrid,BorderLayout.NORTH);
		
		//Add the main window to the JFrame
		this.add(window);
	}
	
	/**
	 * Add the panel containing the drag and drop emojis
	 */
	private void addEmojiPanel() {
		//Create tabs to separate emojis into categories
		tabbedPane = new JTabbedPane();
		tabbedPane.setPreferredSize(new Dimension(600, 400));
		
		//Add emojis to the tabs
		JScrollPane faces = getEmojiPane(128512, 128590);
		JScrollPane animals = getEmojiPane(128000, 128063);
		JScrollPane food = getEmojiPane(127813, 127871);
		JScrollPane activities = getEmojiPane(127904, 127955);
		JScrollPane vehicles = getEmojiPane(128640, 128676);
		
		//Add the tabs to the window
		tabbedPane.add("Faces", faces);
		tabbedPane.add("Animals", animals);
		tabbedPane.add("Food and Drink", food);
		tabbedPane.add("Activities", activities);
		tabbedPane.add("Vehicles", vehicles);
		window.add(tabbedPane, BorderLayout.SOUTH);
	}
	
	/**
	 * Create a JScrollPane and adds drag and drop emojis to it. 
	 * A range (inclusive) of decimal Unicode values must be specified to be added.
	 * @param start		the start of the range of Unicode values that will be added 
	 * @param end		the end of the range of Unicode values that will be added
	 */
	private JScrollPane getEmojiPane(int start, int end) {
		JPanel emojiPanel = new JPanel();
		emojiPanel.setPreferredSize(new Dimension(600, 600));
		JScrollPane scroll = new JScrollPane(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scroll.setViewportView(emojiPanel);
		
		//Loop through the entire specified range
		for (int i = start; i <= end; i ++) {
			//Convert the decimal value to its character representation
			String unicodeChar = UnicodeConverter.getJavaUnicodeChar(i);
			//Buttons will be used as the drag and drop components
			//Create the button with the character as its text
			JButton emoji = new JButton(unicodeChar);
			//Change the font of the button to one that can display emojis
			emoji.setFont(new Font("Segoe UI Emoji", Font.PLAIN, 40));
			//Make the button transparent
			emoji.setContentAreaFilled(false);
			//Make the buttons evenly sized
			emoji.setPreferredSize(new Dimension(90, 90));
			emojiPanel.add(emoji);
			//Get the Unicode hex of the mapping so it can be transferred
			String mapping = UnicodeConverter.convertIntToHex(i);
			
			emoji.setToolTipText("<html>Unicode: " + mapping
					+ "<br>Drag onto key to set mapping</html>");
			
			//Set the transfer handler of the button
            emoji.setTransferHandler(new ValueExportTransferHandler(mapping));
            emoji.addMouseMotionListener(new MouseAdapter() {
                @Override
                public void mouseDragged(MouseEvent e) {
                	//Mouse drag has been detected, get the button
                    JButton button = (JButton) e.getSource();
                    //Use transfer handler to export the Unicode hex
                    TransferHandler handle = button.getTransferHandler();
                    handle.exportAsDrag(button, e, TransferHandler.COPY);
                }
            });
		}
		
		return scroll;
	}
	
	/**
	 * Adds the graphical representation of the keymap tree. 
	 * This allows visualisation and selection of the keymaps in the model.
	 */
	private void addKeymapTree() {
		//Get the JTree containing the maps
		tree = presenter.buildJTree();
		
		//Only allow one map to be selected at a time
		tree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
		//Listen for selection
		tree.addTreeSelectionListener(this);
	
		//Create a mouse listener for the tree
		MouseListener ml = new MouseAdapter() {
		     public void mousePressed(MouseEvent e) {
		    	 //The tree has been clicked
		    	 //Get the node that has been selected
		         int selRow = tree.getRowForLocation(e.getX(), e.getY());
		         //Check if a valid node has been selected
		         if(selRow != -1) {
		        	 //If the mouse event was a right click, activate the right click popup
		             if(SwingUtilities.isRightMouseButton(e)) {
		            	 keymapRightClick(e.getX(), e.getY(), e.getComponent());
		             }
		         }
		     }
		 };
		 //Add the mouse listener to the tree
		 tree.addMouseListener(ml);
		
		 //Put the tree in a scroll pane
		treeView = new JScrollPane(tree);
		//Expand all the nodes by default
		for (int i = 0; i < tree.getRowCount(); i++) {
		    tree.expandRow(i);
		}
		
		//Add the tree to the window
		window.add(treeView, BorderLayout.WEST);
	}
	
	/**
	 * Handle right click on the JTree.
	 * @param x			x coordinate of the right click
	 * @param y			y coordinate of the right click
	 * @param component	the component that has been clicked
	 */
	private void keymapRightClick(int x, int y, Component component) {
		//Get the path of the node that has been selected
		TreePath selPath = tree.getPathForLocation(x, y);
		//Use the path get the node
		selectedNode = (DefaultMutableTreeNode) selPath.getLastPathComponent();
		
		//Show the right click options menu
		keymapOptionsMenu.show(component, x, y);
	}
	
	/**
	 * Add a node to the keymap JTree.
	 * @param name	the name of the keymap to be added to the tree
	 */
	public void addKeymapNode(String name) {
		//Get the tree model
		DefaultTreeModel model = (DefaultTreeModel) tree.getModel();
		//Add the keymap as a child of the selected node
		selectedNode.add(new DefaultMutableTreeNode(name));
		//Reload the model to acvtivate changes
		model.reload();
	}
	
	/**
	 * Add listeners to the keyboard buttons.
	 */
	private void addListeners() {
		//Loop through all the keyboard buttons
		for (HashMap.Entry<JButton, String> entry : buttonKeyPairings.entrySet()) {
			JButton button = entry.getKey();
			//Add an action listener to respond to click events
			button.addActionListener(this);
			//Add a transfer handler to respond to drag and drop events
			button.setTransferHandler(new ValueImportTransferHandler());
		}
	}
	
	/**
	 * Sets the image display of a keyboard key
	 * @param key	the unique identifier of the key to place the image on
	 * @param img	the image to be displayed
	 */
	public void setKeyImage(String key, BufferedImage img) {
		//Get the button from the identifier
		JButton button = getButton(key);
		//Resize the image temporarily to fit the button
		Image tmp = img.getScaledInstance(50, 50, Image.SCALE_SMOOTH);
	    BufferedImage dimg = new BufferedImage(50, 50, BufferedImage.TYPE_INT_ARGB);
	    Graphics2D g2d = dimg.createGraphics();
	    g2d.drawImage(tmp, 0, 0, null);
	    g2d.dispose();
		//Display the image on the button
		button.setIcon(new ImageIcon(dimg));
		button.setText(null);
	}

	/**
	 * Helper method to get a specified keyboard button
	 * @param keyString	the unique identifier of the key
	 * @return			the button relating to the specified key
	 */
	private JButton getButton(String keyString) {
		//Loop through all the button in the map
		for (HashMap.Entry<JButton, String> entry : buttonKeyPairings.entrySet()) {
			JButton key = entry.getKey();
		    String value = entry.getValue();
		    
		    //If its the correct key, return the button
		    if (value.equals(keyString)) {
		    	return key;
		    }
		}
		//Button can't be found, return null
		return null;
	}
	
	/**
	 * Set the border of a specified keyboard button. 
	 * A null argument for border will remove the current border.
	 * @param key		the unique identifier of the keyboard key
	 * @param border	the colour of the border
	 */
	public void setKeyBorder(String key, Color border) {
		//Get the button relating to the key
		JButton button = getButton(key);
		//Null argument, remove the border
		if (border == null)
			button.setBorder(null);
		//Set the border to the specified button
		else 
			button.setBorder(BorderFactory.createLineBorder(border, 5));
	}
	
	/**
	 * Returns whether the specified key exists on the virtual keyboard.
	 * @param key	the unique identifier of the key
	 * @return		returns <code>true</code> if virtual keyboard contains, 
	 * 				the specified key, <code>false</code> otherwise
	 */
	public boolean hasKey(String key) {
		//Check if the key is in the map
		return buttonKeyPairings.containsValue(key);
	}
	
	/**
	 * Set the text of a key on the virtually keyboard.
	 * @param key			the unique identifier of the key to be modified
	 * @param displayText	the text to be displayed on the button
	 */
	public void setKeyDisplayText(String key, String displayText) {
		//Get the button relating to the key
		JButton button = getButton(key);
		//Remove any current image
		button.setIcon(null);
		//Check if the display text is an emoji
		if (displayText != null && !(displayText.length() > 1 && Character.isSurrogatePair(displayText.charAt(0), displayText.charAt(1)))) {
			//If it's not an emoji, set the text to the default
			button.setFont(new Font(Font.DIALOG, Font.PLAIN, 15));
		} else {
			//The display text is an emoji, use the font that supports emojies
			button.setFont(new Font("Segoe UI Emoji", Font.PLAIN, 18));
		}
		//Set the button text
		button.setText(displayText);
	}
	
	/**
	 * Returns the virtual keyboard to its default display. The default 
	 * display is the text assigned to the keys when they are first created 
	 * in @see KeyboardDisplay.Class. Borders and images are also removed.
	 */
	public void setDefaultDisplay() {
		//Loop through all the buttons on the virtual keyboard
		for (JButton button : buttonKeyPairings.keySet()) {
			//Set the button text to the default
			button.setText(buttonDefaultDisplay.get(button));
			//Remove button borders
		    button.setBorder(null);
		    //Remove image overlays
		    button.setIcon(null);
		    //Restore font to the default
		    button.setFont(new Font(Font.DIALOG, Font.PLAIN, 15));
		}
	}
	
	/**
	 * Removes the display from a key and restores it to the default
	 * @param key	the key identifier
	 */
	public void removeKeyDisplay(String key) {
		//Find the key in the map
		for (HashMap.Entry<JButton, String> entry : buttonKeyPairings.entrySet()) {
		    JButton button = entry.getKey();
		    String identifier = entry.getValue();
		    //Restore the text to default
		    if (key.equals(identifier)) {
		    	button.setText(buttonDefaultDisplay.get(button));
		    	button.setIcon(null);
		    }
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		//Get the source of the action
		Object source = e.getSource();
		//The generate XML button has been pressed
		if (source == xml) {
			//Generate the XML file
			generateXML();
		//The user wishes to reset the keymap
		} else if (source == reset) {
			resetDisplay();
		//One of the buttons on the virtual keyboard has been pressed
		} else if(source == load) {
			loadKeymap();
		} else if (source instanceof JButton) {
			//Get the button that has been pressed
			JButton pressedButton = (JButton) source;
			//Open the advanced mapping popup
			openMappingPopup(pressedButton);
		}
		
	}
	
	/**
	 * Loads a keymaps from an XML file
	 */
	private void loadKeymap() {
		//Set up the file chooser
		fileChooser = new JFileChooser();
		FileFilter filter = new FileNameExtensionFilter("XML files", "xml");
		fileChooser.setFileFilter(filter);
		
		//Loads the main GUI displaying the keymaps in the selected file
		int result = fileChooser.showOpenDialog(new JFrame());
		//If valid file, get the path and load the keymaps
    	if (result == JFileChooser.APPROVE_OPTION) {
    	    File selectedFile = fileChooser.getSelectedFile();
	    	presenter.loadExistingFile(selectedFile.getAbsolutePath());
	    	//Close the current window
	    	dispose();
    	};
	}
	
	/**
	 * Removes all mappings and displays from the keys in the key map
	 */
	private void resetDisplay() {
		//Inform the user that the display is about to be reset
		int n = JOptionPane.showConfirmDialog(null, "Are you show "
				+ "you want to reset the keymap? This will delete "
				+ "your mappings", "Reset Keymap", JOptionPane.YES_NO_OPTION);
		//If the user confirms and a keymap is selected, reset the keymap
		if (n == JOptionPane.YES_OPTION && currentKeymap != null) {
			//Reset
			presenter.resetKeymap(currentKeymap);
		}
	}
	
	/**
	 * Opens the advanced mapping popup window
	 * @param pressedButton	the button that has been pressed
	 */
	private void openMappingPopup(JButton pressedButton) {
		//Check button has been correctly labelled
		if (buttonKeyPairings.containsKey(pressedButton)) {
			//Get the unique identifier of the key
			String selectedKey = buttonKeyPairings.get(pressedButton);
			
			//Ensure that a keymap is selected
			if (currentKeymap != null) {
				//Open the advanced mapping popup window
				presenter.openMappingPopup(selectedKey, currentKeymap);
			} else {
				//Tell the user to select a keymap
				JOptionPane.showMessageDialog(this, "You have not selected a keymap.", "error", JOptionPane.PLAIN_MESSAGE);
			}
		}
	}
	
	/**
	 * Generates the mappings XML in a designated directory
	 */
	private void generateXML() {
		//Open the file chooser to let the user pick the location
		//Set up the file chooser
		chooser = new JFileChooser();
		chooser.setFileFilter( new FolderFilter() );
		chooser.setDialogTitle("Pick Output location");
		//disable the "All files" option.
		//chooser.setAcceptAllFileFilterUsed(false);
		chooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
		
		String path = "";
		
		//Get the chosen path of the output directory
		if (chooser.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) { 
			File file = chooser.getSelectedFile();
			path = file.getPath();
			
			//Generate the XML
			presenter.generateXML(path);
		}
	}
	
	/**
	 * Listen for changes to the keymap JTree.
	 */
	public void valueChanged(TreeSelectionEvent e) {
		//Returns the last path element of the selection.
		DefaultMutableTreeNode node = (DefaultMutableTreeNode) tree.getLastSelectedPathComponent();

	    if (node == null)
	    //Nothing is selected.     
	    return;

	    //Get the keymap that was selected
	    Object nodeInfo = node.getUserObject();
	    String keymap;
	    keymap = (String)nodeInfo;
	    //Make sure selection is a keymap and not the top level container
	    if (keymap != "Keymaps") {
	    	presenter.loadKeyMap(keymap); 
	    } 
	}
	
	/**
	 * Sets the current keymap that is displayed in the view.
	 * @param keymap	the name of the keymap that will be displayed
	 */
	public void setCurrentKeymap(String keymap) {
		currentKeymap = keymap;
	}
	
	/**
	 * Helper class for drag and drop components. Handles the export of a String 
	 * from a component. This class must be set as the transfer handler for 
	 * the component.
	 * @author Matthew Henderson - University of Glasgow Masters Project 2018
	 *
	 */
	private class ValueExportTransferHandler extends TransferHandler {

	    private String value;

	    public ValueExportTransferHandler(String value) {
	    	//The unicode hex to be transfered
	        this.value = value;
	    }

	    public String getValue() {
	        return value;
	    }

	    @Override
	    public int getSourceActions(JComponent c) {
	        return DnDConstants.ACTION_COPY_OR_MOVE;
	    }

	    @Override
	    protected Transferable createTransferable(JComponent c) {
	        Transferable t = new StringSelection(getValue());
	        return t;
	    }

	    @Override
	    protected void exportDone(JComponent source, Transferable data, int action) {
	        super.exportDone(source, data, action);
	        // Decide what to do after the drop has been accepted
	    }

	}
	
	/**
	 * Helper class for drag and drop components. Handles the import of a String to a 
	 * component. This class must be set as the transfer handler for the component.
	 * @author Matthew Henderson - University of Glasgow Masters Project 2018
	 *
	 */
	private class ValueImportTransferHandler extends TransferHandler {

	    public final DataFlavor SUPPORTED_DATA_FLAVOR = DataFlavor.stringFlavor;

	    public ValueImportTransferHandler() {
	    }

	    @Override
	    public boolean canImport(TransferHandler.TransferSupport support) {
	        return support.isDataFlavorSupported(SUPPORTED_DATA_FLAVOR);
	    }

	    @Override
	    public boolean importData(TransferHandler.TransferSupport support) {
	        boolean accept = false;
	        //Check the data flavour is compatible
	        if (canImport(support)) {
	            try {
	            	//Get the value of transfer data
	                Transferable t = support.getTransferable();
	                Object value = t.getTransferData(SUPPORTED_DATA_FLAVOR);
	                if (value instanceof String) {
	                	//Get the component that was dragged
	                    Component component = support.getComponent();
	                    //Check that the data came from a button
	                    if (component instanceof JButton) {
	                    	//Get the button that was dragged
	                    	JButton button = (JButton) component;
	                    	//Get the unique identifier of the key that the button relates to
	                        String selectedKey = buttonKeyPairings.get(button);
	                        //Set the mapping if the hex string is valid
	                        if (UnicodeConverter.isUnicodeHexString(value.toString())) {
	                        	presenter.setKeyMapping(selectedKey, currentKeymap, value.toString());
	                        } 
	                        accept = true;
	                    }
	                }
	            } catch (Exception exp) {
	                exp.printStackTrace();
	            }
	        }
	        return accept;
	    }
	}
	
	/**
	 * Custom file filter to only show directories
	 * @author Matthew Henderson - University of Glasgow Masters Project 2018
	 *
	 */
	private static class FolderFilter extends FileFilter {
	      @Override
	      public boolean accept( File file ) {
	        return file.isDirectory();
	      }

	      @Override
	      public String getDescription() {
	        return "Directories";
	      }
	    }
}