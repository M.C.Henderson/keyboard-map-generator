package mapping;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * Class to model a k-ary tree. This is a tree data structure where each node 
 * can have multiple children. It was created to model the structure that the 
 * keymaps will take within the XML file. The tree implements a breadth first 
 * iterator.
 * @author Matthew Henderson - University of Glasgow Masters Project 2018
 *
 * @param <T>	the data type to be contained in the tree
 */
public class TreeDataStructure<T extends Comparable<T>> implements Iterable<T> {
	private TreeNode<T> root;
	
	/**
	 * Constructs the TreeDataStructure object. 
	 * Places an empty node at the root of the tree. This allows multiple KeyMap to 
	 * be at the top level of the hierarchy (the empty root node is ignored and its 
	 * children are considered the top of the tree).
	 */
	public TreeDataStructure() {
		this.root = new TreeNode<T>(null, null);
	}
	
	/**
	 * Returns whether the tree is empty, the root node is ignored.
	 * @return	returns <code>true</code> if the tree is empty,  
	 * 			<code>false</code> otherwise
	 */
	public boolean isEmpty() {
		return !root.hasChildren();
	}

	/**
	 * Removes an element from the tree.
	 * @param element	the element to be removed
	 * @return			returns <code>true</code> if the elements has been   
	 * 					successfully removed, <code>false</code> otherwise
	 */
	public boolean remove(T element) {
		
		//If the element is null return false
		if (element == null) {
			return false;
		}
		
		//Create a stack to store unvisited nodes
		LinkedList<TreeNode<T>> stack = new LinkedList<TreeNode<T>>();
		//Start with the root
		stack.add(root);
		
		//If the stack is empty, the element is not in the tree
		while (!stack.isEmpty()) {
			//Pop the top element
			TreeNode<T> node = stack.pop();
			//The element is in the tree
			if (node.getData() != null && element.equals(node.getData())) {
				//Get the elements parent and remove the element from the tree
				TreeNode<T> parent = node.getParent();
				parent.removeChild(element);
				return true;
			//The element does not match the current node
			} else {
				//Put the node's children in the stack
				List<TreeNode<T>> children = node.getChildren();
				for (TreeNode<T> item : children) {
					stack.add(item);
				}
			}
		}
		return false;
	}
	
	/**
	 * Returns whether an element is in the tree.
	 * @param element	the element to be checked for
	 * @return			returns <code>true</code> if the element is in the   
	 * 					tree, <code>false</code> otherwise
	 */
	public boolean contains(T element) {
		
		//If the elements is null return false
		if (element == null) {
			return false;
		}
		
		//Create a stack to store unvisited nodes
		LinkedList<TreeNode<T>> stack = new LinkedList<TreeNode<T>>();
		//Start with the root
		stack.add(root);
		
		//If the stack is empty, the element is not in the tree
		while (!stack.isEmpty()) {
			//Pop the top element
			TreeNode<T> node = stack.pop();
			//The element is in the tree, return true
			if (node.getData() != null && element.equals(node.getData())) {
				return true;
			//The element does not match the current node
			} else {
				//Put the node's children in the stack
				List<TreeNode<T>> children = node.getChildren();
				for (TreeNode<T> item : children) {
					stack.add(item);
				}
			}
		}
		return false;
	}
	
	/**
	 * Adds an element to the tree.
	 * @param element	the element to be added
	 * @param parent	the parent of the element to be added
	 * @return			returns <code>true</code> if the element has been added to the  
	 * 					tree, <code>false</code> otherwise
	 */
	public boolean add(T element, T parent) {
		
		//If the element is null return false
		if (element == null) {
			return false;
		}
		
		//If the parent is null, add the element to the root
		if (parent == null) {
			root.addChild(element);
			return true;
		}
		
		//Create a stack to store unvisited nodes
		LinkedList<TreeNode<T>> stack = new LinkedList<TreeNode<T>>();
		//Start with the root
		stack.add(root);
		
		//If the stack is empty, the parent is not in the tree
		while (!stack.isEmpty()) {
			//Pop the top element
			TreeNode<T> node = stack.pop();
			//The parent is in the tree, add the element as a child and return true
			if (node.getData() != null && parent.equals(node.getData())) {
				node.addChild(element);
				return true;
			//The parent does not match the current node
			} else {
				//Put the node's children in the stack
				List<TreeNode<T>> children = node.getChildren();
				for (TreeNode<T> item : children) {
					stack.add(item);
				}
			}
		}
		return false;
	}
	
	/**
	 * Returns the parent of the element in the tree.
	 * @param element	the element thats parent will be retrieved
	 * @return			the parent of the element
	 */
	public T getParent(T element) {
		
		//If the element is null return false
		if (element == null) {
			return null;
		}
		
		//Create a stack to store unvisited nodes
		LinkedList<TreeNode<T>> stack = new LinkedList<TreeNode<T>>();
		//Start with the root
		stack.add(root);
		
		//If the stack is empty, the element is not in the tree
		while (!stack.isEmpty()) {
			//Pop the top element
			TreeNode<T> node = stack.pop();
			//The element is in the tree, return its parent
			if (node.getData() != null && element.equals(node.getData())) {
				TreeNode<T> parent = node.getParent();
				return parent.getData();
			//The parent does not match the current node
			} else {
				//Put the node's children in the stack
				List<TreeNode<T>> children = node.getChildren();
				for (TreeNode<T> item : children) {
					stack.add(item);
				}
			}
		}
		
		return null;
	}
	
	/**
	 * Returns a breadth-first iterator.
	 * @return	the iterator
	 */
	@Override
	public Iterator<T> iterator() {
		return new PreOrderIterator();
	}
	
	/**
	 * Breadth first iterator for the TreeDataStructure.
	 * @author Matthew Henderson - University of Glasgow Masters Project 2018
	 *
	 */
	private class PreOrderIterator implements Iterator<T> {
		
		private LinkedList<TreeNode<T>> stack;
		private LinkedList<T> output;
		
		/**
		 * Constructs the iterator.
		 */
		private PreOrderIterator() {
			stack = new LinkedList<TreeNode<T>>();
			output = new LinkedList<T>();
			
			//Start with the root
			stack.add(root);
			
			//loop while the stack isn't empty
			while (!stack.isEmpty()) {
				//pop the top of the stack
				TreeNode<T> node = stack.pop();
				//Only add to the output if the node has data
				//this ignores the root node
				if (node.getData() != null) {
					//Add to a list for output
					output.add(node.getData());
				}
				//Add the nodes children to the stack
				List<TreeNode<T>> children = node.getChildren();
				for (TreeNode<T> item : children) {
					stack.add(item);
				}
			}
		}

		/**
		 * Returns whether the iterator has another element.
		 * @return	returns <code>true</code> if there is another element 
		 * 			tree, <code>false</code> otherwise
		 */
		@Override
		public boolean hasNext() {
			return (!output.isEmpty());
		}

		/**
		 * Returns the next element in the iterator.
		 * @return	the next element in the iterator
		 */
		@Override
		public T next() {
			//If there is not another node, throw an exception
			if (output.isEmpty())
		        throw new NoSuchElementException();
			
			//Return the top of the output list
			T place = output.pop();
			return place;
		}
	}
}
