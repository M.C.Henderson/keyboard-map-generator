package mapping;
import java.awt.image.BufferedImage;

/**
 * Class to represent a key on the keyboard. Each key belongs to a KeyMap object. 
 * The key is identified by the unique key identifier, this is a combination of 
 * the name and location.
 * @author Matthew Henderson - University of Glasgow Masters Project 2018
 */
public class Key { 
	
	private boolean isTrigger;
	private boolean isMapped;
	private boolean isRandomised;
	private DisplayType displayType;
	private String keyName;
	private String location;
	private String mappingUnicode;
	private String textOverlay;
	private BufferedImage img;
	private int vkKeyCode;
	
	/**
	 * Enums to represent the display that has been assign to the key.
	 * The TEXT enum means that a text overlay has been assigned to the key.
	 * The IMAGE enum means that an image overlay has been assigned to the key.
	 * The DEFAULt enum means that no overlay has been assigned and instead the
	 * default character representation of the mapping will be used.
	 * @author Matthew Henderson - University of Glasgow Masters Project 2018
	 */
	public enum DisplayType {
		DEFAULT, TEXT, IMAGE
	}
	
	/**
	 * Constructs the key object
	 * @param keyName	the name of the key as defined by the java KeyEvent class
	 * 					e.g. ctrl, home, a...
	 * @param location	the location of the key on the keyboard according the
	 * 					java KeyEvent class e.g left, right, numpad...
	 * @param keyCode	the virtual key code that is used to identify the key 
	 * 					in the windows operating system
	 */
	public Key(String keyName, String location, int keyCode) {
		this.location = location;
		this.keyName = keyName;
		this.vkKeyCode = keyCode;
		isMapped = false;
		isRandomised = false;
		displayType = DisplayType.DEFAULT;
	}
	
	/**
	 * Returns whether key is assigned as the trigger of the keymap.
	 * @return	returns <code>true</code> if the key is the trigger, 
	 * 			<code>false</code> otherwise
	 */
	public boolean isTrigger() {
		return isTrigger;
	}
	
	/**
	 * Returns whether the key is mapped.
	 * @return	returns <code>true</code> if the key is mapped,
	 * 			<code>false</code> otherwise
	 */
	public boolean isMapped() {
		return isMapped;
	}
	
	/**
	 * Returns whether the key is randomised.
	 * @return	returns <code>true</code> if the key is randomised,
	 * 			<code>false</code> otherwise
	 */
	public boolean isRandomised() {
		return isRandomised;
	}
	
	/**
	 * Returns the VK key code of the key. This is the code that identifies the 
	 * key in the windows operating system. Having this information is useful for 
	 * identifying and overwriting key presses. The value is sometimes quoted as a 
	 * hex string. However, the integer representation was chosen here to avoid
	 * confusion with the default Unicode value of the key.
	 * @return	the VK key code of the key
	 */
	public int getVkKeyCode() {
		return vkKeyCode;
	}
	
	/**
	 * Returns the enum which describes the way the key will be displayed. 
	 * DEFAULT means the mappings default unicode character will be displayed, 
	 * TEXT means that that user defined text will be overlayed on the key and 
	 * IMAGE means that an image the user has uploaded will be displayed.
	 * @return	the display type
	 */
	public DisplayType getDisplayType() {
		return displayType;
	}
	
	/**
	 * Returns the location of the key on the keyboard. 
	 * Possible locations are standard, unknown, left, right and numpad. 
	 * @return	the location of the key on the keyboard
	 */
	public String getLocation() {
		return location;
	}
	
	/**
	 * Returns the user defined text overlay for the key.
	 * @return the text overlay
	 */
	public String getTextOverlay() {
		return textOverlay;
	}
	
	/**
	 * Returns the name of the keyboard key. 
	 * This is defined by java's own key names found in the native 
	 * KeyEvent class.
	 * @return	the name of the key
	 */
	public String getKeyName() {
		return keyName;
	}
	
	/**
	 * Returns the image that the user has defined to be displayed on the 
	 * mapped key.
	 * @return	the image to be displayed
	 */
	public BufferedImage getImage() {
		return img;
	}
	
	/**
	 * Returns the unicode that the key has been mapped to.
	 * @return	the unicode mapping
	 */
	public String getMappingUnicode() {
		return mappingUnicode;
	}
	
	/**
	 * Get the String used to uniquely identify the key. 
	 * This is a combination of the key name and the key location.
	 * @return	the unique String used to identify the key
	 */
	public String getUniqueKeyIdentifier() {
		return keyName + " " + location;
	}
	
	/**
	 * Set the image overlay associated the key.
	 * @param img	the image to be set as overlay for the key
	 */
	public void setImage(BufferedImage img) {
		this.img = img;
		displayType = DisplayType.IMAGE;
	}
	
	/**
	 * Set the unicode character that the key will be mapped to.
	 * @param unicode	the unicode character as a unicode hex String
	 */
	public void setMapping(String unicode) {
		this.mappingUnicode = unicode;
		isMapped = true;
	}
	
	/**
	 * Set the key as a trigger key for the keyboard map.
	 * Trigger keys are ones that activate the mapping when pressed.
	 */
	public void setAsTrigger() {
		isTrigger = true;
	}
	
	/**
	 * Sets the key as randomised. This will ensure the key is marked
	 * as randomised in the XML document that is output by the programme.
	 */
	public void randomise() {
		isRandomised = true;
		displayType = DisplayType.DEFAULT;
	}
	
	/**
	 * Sets the text overlay associated with the key.
	 * @param textOverlay	the text to be set as the overlay for the key
	 */
	public void setTextOverlay(String textOverlay) {
		this.textOverlay = textOverlay;
		displayType = DisplayType.TEXT;
	}
	
	/**
	 * Removes any display associated with the key.
	 */
	public void removeCustomDisplay() {
		img = null;
		textOverlay = null;
		displayType = DisplayType.DEFAULT;
	}
	
	/**
	 * Returns whether the display type of the key is set to text overlay.
	 * @return 	returns <code>true</code> if the display type is set to text  
	 * 			overlay, <code>false</code> otherwise
	 */
	public boolean isTextOverlay() {
		if (displayType == DisplayType.TEXT) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Returns whether the display type of the key is set to image overlay.
	 * @return	returns <code>true</code> if the display type is set to image  
	 * 			overlay, <code>false</code> otherwise
	 */
	public boolean isImageOverlay() {
		if (displayType == DisplayType.IMAGE) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Returns whether the display type of the key is set to the default character
	 * representation of the mapping unicode.
	 * @return	returns <code>true</code> if the display type is set to default  
	 * 			character representation, <code>false</code> otherwise.
	 */
	public boolean isDefaultDisplay() {
		if (displayType == DisplayType.DEFAULT) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Removes all modification from the key. Completely resets to default state.
	 */
	public void restoreToDefault() {
		isMapped = false;
		isTrigger = false;
		isRandomised = false;
		mappingUnicode = null;
		removeCustomDisplay();
	}
}

